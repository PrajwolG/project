<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOurclientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ourclients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('group');
            $table->string('file');
            $table->string('title');
            $table->string('status')->nullable();
            $table->string('rank')->nullable();
            $table->string('caption')->nullable();
            $table->string('keywords')->nullable();
            $table->string('metaTag')->nullable();
            $table->string('metaDescription')->nullable();
            $table->integer('user_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ourclients');
    }
}
