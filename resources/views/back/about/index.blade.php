@extends('back.layout.master')
@section('content')
  <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
              <div class="row">
                <div class="col-md-6">
                  <li><i class="fa fa-home"></i><a href="{{('admin')}}">Home</a> | About</li>
                </div>
              </div>
            </ol>
          </div>
        </div>

        @foreach($rows as $data)
        <form method="post" action="{{url('/about/update')}}/{{$data->id}}">
          {{csrf_field()}}
        <div class="row">
          <!-- Left Column Start -->
            <!-- About Start -->

          <div class="col-lg-8">
            <div class="form-wrapper well">
              <div class="form-group">
                
                <!-- Title Start -->
                  <label for="text">Title</label>
                  <input type="text" class="form-control" id="text" placeholder="Enter Title" name="title" value="{{$data->title}}">
                <!-- End of Title -->
                <br>
                
                <!-- Description Start -->
                  <label for="text">Description</label>
                  <textarea class="ckeditor" name="description">{{$data->description}}</textarea>
                <!-- End of Description -->
                <br>

               <br>
                
                <!-- Sub Description Start -->
                  <label for="text">Sub Description</label>
                  <textarea class="ckeditor" name="subdescription">{{$data->subdescription}}</textarea>
                <!-- End of Sub Description -->
                <br>


              </div>
            </div>

              <!-- End of About -->

              <br>  
          </div>

          <!-- End of Left Column -->

          <!-- Right Column Start -->
          <div class="col-lg-4">
            <!-- SEO Bar Start -->
            <div class="row">
              <div class="col-lg-12">
                <ol class="breadcrumb">
                  <div class="row">
                    <div class="col-md-12">
                      <li><i class="fa fa-search"></i>SEO Management</li>
                    </div>
                  </div>
                </ol>
              </div>
            </div>
            <!-- End of SEO Bar -->

            <!-- SEO Management Start -->
            <div class="form-wrapper well">
              <div class="form-group">

                <!-- Caption -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Caption</label>
                  </div>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="text" placeholder="Enter Caption" name="caption" value="{{$data->caption}}">
                  </div>
                </div>
                <!-- End of Caption -->
                <br>
                
                <!-- Keywords -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Keywords</label>
                  </div>
                  <div class="col-sm-8">
                      <textarea class="form-control" id="text" placeholder="Enter Keywords" name="keywords" rows="5">{{$data->keywords}}</textarea>
                  </div>
                </div>
                <!-- End of Keywords -->
                <br>

                <!-- Meta Tag -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Meta Tag</label>
                  </div>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="text" placeholder="Enter Meta Tag" name="metaTag" value="{{$data->metaTag}}">
                  </div>
                </div>
                <!-- End of Meta Tag -->
                <br>

                <!-- Meta Description -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Meta Description</label>
                  </div>
                  <div class="col-sm-8">
                      <textarea class="form-control" id="text" placeholder="Enter Meta Description" name="metaDescription" rows="5">{{$data->metaDescription}}</textarea>
                  </div>
                </div>
                <!-- End of Meta Description -->
                <br>

              </div>
              </div>
            <!-- End of SEO Management -->
              <br>
          </div>
          <!-- End of Right Column -->
        </div>
        <!-- End of Main Row -->   
        
        <br>

        <!-- Button Operation Start -->
        <div class="row">
              <div class="col-lg-12">
                <ol class="breadcrumb">
                  <div class="row">
                    <div class="col-md-12">
                      <center><button class="btn btn-primary"><b>Update</b></button></center>
                    </div>
                  </div>
                </ol>
              </div>
        </div>
        </form>
        @endforeach
        <!-- End of Button Operation -->

        </div>
      </section>
    </section>
@endsection
