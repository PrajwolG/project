@extends('back.team.index')
  @section('team')<!--main content start-->
    <div class="col-lg-4">
        <!-- D-team Bar Start -->
        <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
              <div class="row">
                <div class="col-md-12">
                  <li><i class="fa fa-search"></i>Team Management</li>
                </div>
              </div>
            </ol>
          </div>
        </div>
        <!-- End of D-team Bar -->

        <!-- D-team Management Start-->
          <!-- SEO Management Start -->
        <form action="{{url('/D-team/store')}}" method="post" enctype="multipart/form-data">
          {{csrf_field()}}
        <div class="row">  
            <div class="form-wrapper well">
              <div class="form-group">

                <!-- Image Upload Start -->
                    <input type="file" name="newfile" class="file">

                    <div class="input-group col-sm-12">
                      <span class="input-group-addon"><i class="fa fa-image"></i></span>
                      <input type="text" class="form-control input-sm" disabled placeholder="Upload">
                      <span class="input-group-btn">
                      <button class="browse btn btn-primary input-sm" type="button"><i class="fa fa-search"></i> Browse</button>
                    </span>
                    </div>
                <!-- End of Image Upload -->

                <br>



                <br>
              </div>
            </div>   
          <br>
        </div>
        <!-- End of SEO Management -->
        <!-- End of D-team Management -->

        <!-- start of details -->
        <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
              <div class="row">
                <div class="col-md-12">
                  <li><i class="fa fa-search"></i>Details</li>
                </div>
              </div>
            </ol>
          </div>
        </div>

        <div class="row">  
            <div class="form-wrapper well">
              <div class="form-group">


                <!-- full name -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Full Name</label>
                  </div>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="text" placeholder="Enter FullName" name="fullname" >
                  </div>
                </div>
                <!-- End of fullname -->
                <br>   

                <!-- position -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Position</label>
                  </div>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="text" placeholder="Enter Position" name="position" >
                  </div>
                </div>
                <!-- End of position -->
                <br> 


                <!-- description -->
                <label for="text">Description</label>
                <div class="row">
<!--                   <div class="col-sm-4">
                      
                  </div> -->
                  <div class="col-sm-12">

                  <textarea class="ckeditor" name="description"></textarea>


                  </div>
                </div>
                <!-- End of description -->
                <br> 



              </div>

            </div>
          </div>

          <br>

          <!-- End of details -->

              <!-- Social Links Start -->
                <!-- Social Links Bar -->
              <div class="row">
                  <div class="col-lg-12">
                    <ol class="breadcrumb">
                      <div class="row">
                        <div class="col-md-12">
                          <li><i class="fa fa-share-alt"></i>Social Links</li>
                        </div>
                      </div>
                    </ol>
                  </div>
              </div>
                <!-- End of Social Links Bar -->

                <!-- Social Links Detail -->
              <div class="form-wrapper well">
                <div class="form-group">

                  
                  <!-- Facebook Page -->
                  <div class="row">
                    <div class="col-sm-3">
                        <label for="text">Facebook</label>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="text" placeholder="Enter Facebook Page Link" name="facebook" >
                    </div>
                  </div>
                  <!-- End of Facebook Page-->
                  <br>

                  <!-- Twitter Page -->
                  <div class="row">
                    <div class="col-sm-3">
                        <label for="text">Twitter</label>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="text" placeholder="Enter Twitter Page" name="twitter" >
                    </div>
                  </div>
                  <!-- End of Twitter -->
                  <br>

                  <!-- Google Plus Page -->
                  <div class="row">
                    <div class="col-sm-3">
                        <label for="text">Google+</label>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="text" placeholder="Enter Google Plus Page" name="googlePlus" >
                    </div>
                  </div>
                  <!-- End of Google Plus -->
                  <br>



                  <!-- Instagram Page -->
                  <div class="row">
                    <div class="col-sm-3">
                        <label for="text">Instagram</label>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="text" placeholder="Enter Instagram Page" name="instagram" >
                    </div>
                  </div>
                  <!-- End of Instagram -->
                  <br>

                </div>
              </div>
              <!-- End of Social Link Detail -->
            <!-- End of Social Links -->


            <br>
        <!-- SEO Bar Start -->
        <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
              <div class="row">
                <div class="col-md-12">
                  <li><i class="fa fa-search"></i>SEO Management</li>
                </div>
              </div>
            </ol>
          </div>
        </div>
        <!-- End of SEO Bar -->

        <!-- SEO Management Start -->
        <div class="row">  
            <div class="form-wrapper well">
              <div class="form-group">
               
                <br>

                <!-- Caption -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Caption</label>
                  </div>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="text" placeholder="Enter Caption" name="caption" >
                  </div>
                </div>
                <!-- End of Caption -->
                <br>
                
                <!-- Keywords -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Keywords</label>
                  </div>
                  <div class="col-sm-8">
                      <textarea class="form-control" id="text" placeholder="Enter Keywords" name="keywords" rows="5"></textarea>
                  </div>
                </div>
                <!-- End of Keywords -->
                <br>

                <!-- Meta Tag -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Meta Tag</label>
                  </div>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="text" placeholder="Enter Meta Tag" name="metaTag" >
                  </div>
                </div>
                <!-- End of Meta Tag -->
                <br>

                <!-- Meta Description -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Meta Description</label>
                  </div>
                  <div class="col-sm-8">
                      <textarea class="form-control" id="text" placeholder="Enter Meta Description" name="metaDescription" rows="5"></textarea>
                  </div>
                </div>
                <!-- End of Meta Description -->
                <br>
                <button class="btn btn-default">Save</button>
              </div>
            </div>   
          <br>
        </div>
        <!-- End of SEO Management -->
        </form>
    </div>    
    @endsection