@extends('back.layout.master')
@section('content')
<!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-laptop"></i>Home</h3>
            <ol class="breadcrumb">
              <li><i class="fa fa-home"></i><a href="{{'admin'}}">Home</a></li>
              <!-- <li><i class="fa fa-laptop"></i>Dashboard</li> -->
            </ol>
          </div>
        </div>
        


        <div class="row">

          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <a href="{{'admin'}}">
              <div class="info-box blue-bg">
                <i class="fa fa-home"></i>
                <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;"> Home </h4>
              </div>
            <a href="{{'slider'}}">
            <!--/.info-box-->
          </div>

          

           <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <a href="{{'slider'}}">
              <div class="info-box green-bg">
                <i class="fa fa-sliders" aria-hidden="true"></i>
                <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;"> Sliders </h4>
              </div>
            </a>
            <!--/.info-box-->
          </div>

          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <a href="{{'about'}}">
              <div class="info-box green-bg">
                <i class="fa fa-sliders" aria-hidden="true"></i>
                <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;"> About </h4>
              </div>
            </a>
            <!--/.info-box-->
          </div>

          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <a href="{{'D-team'}}">
              <div class="info-box blue-bg">
                <i class="fa fa-group"></i>
                <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;"> Team </h4>
              </div>
            <!--/.info-box-->
          </div>

          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <a href="{{'service'}}">
              <div class="info-box blue-bg">
                <i class="fa fa-support"></i>
                <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;"> Service </h4>
              </div>
            <!--/.info-box-->
          </div>

          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <a href="{{'ourclients'}}">
              <div class="info-box blue-bg">
                <i class="fa fa-user"></i>
                <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;"> Our Clients</h4>
              </div>
            <!--/.info-box-->
          </div>

          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <a href="{{'testimonial'}}">
              <div class="info-box blue-bg">
                <i class="fa fa-user"></i>
                <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;">Testimonial</h4>
              </div>
            <!--/.info-box-->
          </div>

          





          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <a href="{{'gallery'}}">
              <div class="info-box green-bg">
                <i class="fa fa-film" aria-hidden="true"></i>
                <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;"> Gallery </h4>
              </div>
            </a>
            <!--/.info-box-->
          </div>
          <!--/.col-->
            
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <a href="{{'career'}}">
              <div class="info-box blue-bg">
                <i class="fa fa-group"></i>
                <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;"> Career </h4>
              </div>
            <!--/.info-box-->
          </div>


        
        
          <!--/.col-->
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <a href="{{'user'}}">
              <div class="info-box brown-bg">
                <i class="fa fa-user" aria-hidden="true"></i>
                <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;"> Users </h4>
              </div>
            </a>
            <!--/.info-box-->
          </div>
          


          <!--/.col-->
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <a href="{{'UserCareer'}}">
              <div class="info-box brown-bg">
                <i class="fa fa-cog" aria-hidden="true"></i>
                <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;"> User Career </h4>
              </div>
            </a>
            <!--/.info-box-->
          </div>

          <!--/.col-->
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <a href="{{'ContactUs'}}">
              <div class="info-box green-bg">
                <i class="fa fa-cog" aria-hidden="true"></i>
                <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;"> Contact Us </h4>
              </div>
            </a>
            <!--/.info-box-->
          </div>
          
          <!--/.col-->
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <a href="{{'setting'}}">
              <div class="info-box dark-bg">
                <i class="fa fa-cog" aria-hidden="true"></i>
                <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;"> Settings </h4>
              </div>
            </a>
            <!--/.info-box-->
          </div>




          

      </section>
    </section>
    <!-- container section start -->
    @endsection