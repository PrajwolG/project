@extends('back.layout.master')
  @section('content')<!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        
      <!-- main bar start -->
      <div class="row">
        <div class="col-lg-12">
          <ol class="breadcrumb">
            <div class="row">
              <div class="col-md-6">
                <li><i class="fa fa-home"></i><a href="{{'admin'}}">Home</a> | Career</li>
              </div>
            </div>
          </ol>
        </div>
      </div>
      <!-- end main bar -->

      <!-- main content start -->
      <form action="{{url('/career/store')}}" method="post" enctype="multipart/form-data">
          {{csrf_field()}}      
      <div class="row">
               
          <!-- Left Column Start -->
            <!-- About Start -->

          <div class="col-lg-8">
            <div class="form-wrapper well">
              <div class="form-group">

                
                <!-- Title Start -->
                  <label for="text">File type</label>
                  <select class="form-control" id="text" name="filetype">
                    <option value="image">Image File</option>
                    <option value="pdf">PDF File</option>
                    <option value="doc">Doc File</option>
                    <option value="video">Video File</option>

                  </select>
<!--                   <input type="text" class="form-control" id="text" placeholder="Enter Title" name="title" >
 -->                <!-- End of Title -->
                <br>
                
                <!-- Description Start -->
                  <label for="text">Description</label>
                  <textarea class="ckeditor" name="description"></textarea>
                <!-- End of Description -->
                <br>

               <br>
                
                <!-- Extra Description Start -->
<!--                   <label for="text">Extra Description</label>
                  <textarea class="ckeditor" name="extraDescription"></textarea>
                <!-- End of Extra Description -->
                <br>


              </div>
            </div>

              <!-- End of career -->

              <br>  
          </div>



          <!-- End of Left Column -->

            
            <!-- Right Column Start -->
              <!-- @yield('service') -->

        <div class="col-lg-4">
        <!-- service Bar Start -->
        <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
              <div class="row">
                <div class="col-md-12">
                  <li><i class="fa fa-search"></i>Career Management</li>
                </div>
              </div>
            </ol>
          </div>
        </div>
        <!-- End of service Bar -->

        <!-- service Management Start-->
        

        <div class="row">  
            <div class="form-wrapper well">
              <div class="form-group">

                <!-- Image Upload Start -->
                    <input type="file" name="file" class="file">

                    <div class="input-group col-sm-12">
                      <span class="input-group-addon"><i class="fa fa-image"></i></span>
                      <input type="text" class="form-control input-sm" disabled placeholder="Upload">
                      <span class="input-group-btn">
                      <button class="browse btn btn-primary input-sm" type="button"><i class="fa fa-search"></i> Browse</button>
                    </span>
                    </div>
                <!-- End of Image Upload -->

                <br>
           </div>
            </div>   
          <br>
        </div>
        
        <!-- End of service Management -->
        
        

        <!-- SEO Bar Start -->
        <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
              <div class="row">
                <div class="col-md-12">
                  <li><i class="fa fa-search"></i>SEO Management</li>
                </div>
              </div>
            </ol>
          </div>
        </div>
        <!-- End of SEO Bar -->

        <!-- SEO Management Start -->
        <div class="row">  
            <div class="form-wrapper well">
              <div class="form-group">
               
                <br>

                <!-- Caption -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Caption</label>
                  </div>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="text" placeholder="Enter Caption" name="caption" >
                  </div>
                </div>
                <!-- End of Caption -->
                <br>
                
                <!-- Keywords -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Keywords</label>
                  </div>
                  <div class="col-sm-8">
                      <textarea class="form-control" id="text" placeholder="Enter Keywords" name="keywords" rows="5"></textarea>
                  </div>
                </div>
                <!-- End of Keywords -->
                <br>

                <!-- Meta Tag -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Meta Tag</label>
                  </div>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="text" placeholder="Enter Meta Tag" name="metaTag" >
                  </div>
                </div>
                <!-- End of Meta Tag -->
                <br>

                <!-- Meta Description -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Meta Description</label>
                  </div>
                  <div class="col-sm-8">
                      <textarea class="form-control" id="text" placeholder="Enter Meta Description" name="metaDescription" rows="5"></textarea>
                  </div>
                </div>
                <!-- End of Meta Description -->
              </div>
            </div>   
          <br>
        </div>
        <!-- End of SEO Management -->
        
    </div>    
            <!-- End of Right Column -->        
      </div> 
      <!-- Button Operation Start -->
        <div class="row">
              <div class="col-lg-12">
                <ol class="breadcrumb">
                  <div class="row">
                    <div class="col-md-12">
                      <center><button class="btn btn-primary"><b>Update</b></button></center>
                    </div>
                  </div>
                </ol>
              </div>
        </div>

             <!-- End of main content -->
             </form>
      </section>
    </section>


    @endsection