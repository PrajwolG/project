@extends('back.layout.master')
  @section('content')<!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        
      <!-- main bar start -->
      <div class="row">
        <div class="col-lg-12">
          <ol class="breadcrumb">
            <div class="row">
              <div class="col-md-6">
                <li><i class="fa fa-home"></i><a href="{{'admin'}}">Home</a> | Our Clients</li>
              </div>

              <div class="col-md-6">
                  <li class="text-right">
                
                    <a href="{{'viewclients'}}"><i class="fa fa-eye"></i>View Clients</a></li>
              </div>
            </div>
          </ol>
        </div>
      </div>
      <!-- end main bar -->

      <!-- main content start -->
      <form action="{{url('/ourclients/update')}}/{{$result->id}}" method="post" enctype="multipart/form-data">
          {{csrf_field()}}      
      <div class="row">
               
          <!-- Left Column Start -->
            <!-- About Start -->

          <div class="col-lg-8">
            <div class="form-wrapper well">
              <div class="form-group">

                
                <!-- Group Start -->
                  <label for="text">Group</label>
                  <select class="form-control" id="text" name="group">
                    <option value="1">Website</option>
                    <option value="2">Hosting</option>
                    <option value="3">Domain</option>
                    <option value="4">Hardware</option>
                  </select>
<!--                   <input type="text" class="form-control" id="text" placeholder="Enter Group" name="group" >
 -->                <!-- End of Group -->
                <br>
                
          

        <!-- Title Start -->
                  <label for="text">Title</label>
                  <input type="text" class="form-control" id="text" placeholder="Enter Title" name="title" value="{{$result->title}}" >
                <!-- End of Title -->
                <br> 

        <!-- Link Start -->
                  <label for="text">Link</label>
                  <input type="text" class="form-control" id="text" placeholder="Enter Link" name="link" value="{{$result->link}}"  >
                <!-- End of Link -->
                <br> 

              </div>
            </div>

              <!-- End of ourClients -->

              <br>  


          </div>








          <!-- End of Left Column -->

            
            <!-- Right Column Start -->
              <!-- @yield('service') -->

        <div class="col-lg-4">
        <!-- service Bar Start -->
        <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
              <div class="row">
                <div class="col-md-12">
                  <li><i class="fa fa-search"></i>Our Clients Management</li>
                </div>
              </div>
            </ol>
          </div>
        </div>
        <!-- End of service Bar -->

        <!-- service Management Start-->
        

        <div class="row">  
            <div class="form-wrapper well">
              <div class="form-group">

                <!-- Image Upload Start -->

                    <center><img src="{{url($result->file)}}" height="80" width="100"></center>
                    <br>
                    <input type="file" name="file" class="file" >
                    <input type="hidden" value="{{$result->file}}" name="image">
                    <div class="input-group col-sm-12">
                      <span class="input-group-addon"><i class="fa fa-image"></i></span>
                      <input type="text" class="form-control input-sm" disabled placeholder="Upload" >
                      <span class="input-group-btn">
                      <button class="browse btn btn-primary input-sm" type="button"><i class="fa fa-search"></i> Browse</button>
                    </span>
                    </div>
                  <br>
                <!-- End of Image Upload -->

                <br>
           </div>
            </div>   
          <br>
        </div>
        
        <!-- End of service Management -->
        
        

        <!-- SEO Bar Start -->
        <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
              <div class="row">
                <div class="col-md-12">
                  <li><i class="fa fa-search"></i>SEO Management</li>
                </div>
              </div>
            </ol>
          </div>
        </div>
        <!-- End of SEO Bar -->

        <!-- SEO Management Start -->
        <div class="row">  
            <div class="form-wrapper well">
              <div class="form-group">
               
                <br>

                <!-- Caption -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Caption</label>
                  </div>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="text" placeholder="Enter Caption" name="caption" value="{{$result->caption}}">
                  </div>
                </div>
                <!-- End of Caption -->
                <br>
                
                <!-- Keywords -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Keywords</label>
                  </div>
                  <div class="col-sm-8">
                      <textarea class="form-control" id="text" placeholder="Enter Keywords" name="keywords" rows="5">{{$result->keywords}}</textarea>
                  </div>
                </div>
                <!-- End of Keywords -->
                <br>

                <!-- Meta Tag -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Meta Tag</label>
                  </div>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="text" placeholder="Enter Meta Tag" name="metaTag" value="{{$result->metaTag}}">
                  </div>
                </div>
                <!-- End of Meta Tag -->
                <br>

                <!-- Meta Description -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Meta Description</label>
                  </div>
                  <div class="col-sm-8">
                      <textarea class="form-control" id="text" placeholder="Enter Meta Description" name="metaDescription" rows="5">{{$result->metaDescription}}</textarea>
                  </div>
                </div>
                <!-- End of Meta Description -->
              </div>
            </div>   
          <br>
        </div>
        <!-- End of SEO Management -->
        
    </div>    
            <!-- End of Right Column -->        
      </div> 
      <!-- Button Operation Start -->
        <div class="row">
              <div class="col-lg-12">
                <ol class="breadcrumb">
                  <div class="row">
                    <div class="col-md-12">
                      <center><button class="btn btn-primary"><b>Update</b></button></center>
                    </div>
                  </div>
                </ol>
              </div>
        </div>

             <!-- End of main content -->
             </form>
      </section>
    </section>


    @endsection