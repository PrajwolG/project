@extends('back.layout.master')
@section('content')
  <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
              <div class="row">
                <div class="col-md-6">
                  <li class="text-left"><i class="fa fa-home"></i><a href="{{('admin')}}">Home</a> | Our Clients</li>             
                </div>
                <!-- <li><i class="fa fa-laptop"></i>Dashboard</li> -->
                

                <div class="col-md-6">
                  <li class="text-right"><a href="{{'ourclients'}}"><i class="fa fa-plus"></i>Add Clients</a></li>
                </div>
             

              </div>
            </ol>


            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>S.N.</th>
                    <th>File</th>
                    <th>Group</th>
                    <th>Title</th>
                    <th>Link</th>
                    <th>Status</th>
                    <th colspan="3" style="text-align: center;">Setting</th>

                    
                  </tr>
                </thead>
                <tbody>
                  
                  @foreach($ourclients as $data)
                  <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td><img src="{{url($data->file)}}" height="80" width="100"></td>
                    <td>{{$data->group}}</td>
                    
                    <td>{!!$data->title!!}</td>
                    <td>{!!$data->link!!}</td>
                    <td>
                      @if($data->status==1)
                      <a href="{{'ourclients/inactivate'}}/{{$data->id}}">
                        <div class="btn btn-default" style=" background-color:red;">InActivate</div>
                      </a>
                      @else
                      <a href="{{'ourclients/activate'}}/{{$data->id}}">
                        <div class="btn btn-default" style=" background-color:lightgreen;">Activate</div>
                      </a>
                      @endif
                    </td>

                    <td style="text-align: center;"><a href="{{url('/ourclients/edit')}}/{{$data->id}}"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a></td>
                    <td style="text-align: center;"><a href="{{('ourclients/delete')}}/{{$data->id}}"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></td>  



                  </tr>
                  @endforeach

                </tbody>
              </table>
            </div>
            <center>
              <div class="col-lg-12">
                {!! $ourclients->render()!!}

              </div>
            </center>

            

          </div>



        </div>


      </section>
    </section>
    <!-- container section start -->
@endsection