@extends('back.layout.master')
@section('content')
  <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
              <div class="row">
                <div class="col-md-6">
                  <li class="text-left"><i class="fa fa-home"></i><a href="{{('admin')}}">Home</a> | Contact Us</li>
                </div>



                <!-- <li><i class="fa fa-laptop"></i>Dashboard</li> -->
                

<!--                 <div class="col-md-6">
                  <li class="text-right"><a href="#"><i class="fa fa-plus"></i>Add</a></li>
                </div> -->
             

              </div>
            </ol>


            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>S.N.</th>
                    <th>Name</th>
                    <th>Contact No.</th>
                    <th>Email ID</th>
                    <th>Message</th>


                    <th colspan="3" style="text-align: center;">Setting</th>
                  </tr>
                </thead>
                <tbody>
                  
                  @foreach($ContactUs as $data)
                  <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$data->name}}</td>
                    <td>{{$data->contactno}}</td>
                    <td>{{$data->email}}</td>
                    <td>{{$data->message}}</td>
<!--                     <td style="text-align: center;"><a href="{{url('/ContactUs/edit')}}/{{$data->id}}"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a></td>
 -->                    <td style="text-align: center;"><a href="{{('ContactUs/delete')}}/{{$data->id}}"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></td>
                  </tr>
                  @endforeach

                </tbody>
              </table>
            </div>
            <center>
              <div class="col-lg-12">
                {!! $ContactUs->render() !!}
<!--                 <ul class="pagination">
                  <li><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">4</a></li>
                  <li><a href="#">5</a></li>
                </ul> -->
              </div>
            </center>

            

          </div>

 

        </div>


      </section>
    </section>
    <!-- container section start -->
@endsection