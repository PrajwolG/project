<html>
<head>
	<title></title>
	@include('front.common.pageheader')
</head>
<body id="home">

@include('front.common.pagenav')

@yield('page')

@include('front.common.footer')

</body>
</html>