 <meta charset="UTF-8">
    <meta name="description" content="A truly IT Company">
    <meta name="keywords" content="KITENepal,KITE Nepal,IT Company,Bhaktapur">
    <meta name="author" content="KITE Nepal">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{url('frontend/img/Kitenepal.png')}}" rel="icon">
    <title>KITE NEPAL, IT club in Nepal</title>
    <link rel="stylesheet" type="text/css" href="{{url('frontend/stylesheets/kitenepal/kitenepal.min.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
    <script type="text/javascript" src="{{url('frontend/js/header.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{url('frontend/stylesheets/main.css')}}">