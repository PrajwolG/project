    <nav id="scroll-nav" class="navbar navbar-inverse navbar-fixed-top trans-back">
        <div class="container-fluid">
            <div class="navbar-header">
              <a class="navbar-brand" href="#"><img src="{{url('frontend/img/Kitenepal.png')}}" class="img-responsive" alt=""></a>
              <div class="navbar-brand">
                <div class="row nav-serv text-center nav-serv--md">
                    <div class="col-sm-3 col-xs-3">
                        <div class="group">
                            <div class="icon"><img src="{{url('frontend/img/icon.png')}}" class="img-responsive" alt=""></div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-3">
                        <div class="group">
                            <div class="icon"><img src="{{url('frontend/img/yoyo.png')}}" class="img-responsive" alt=""></div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-3">
                        <div class="group">
                            <div class="icon"><img src="{{url('frontend/img/hdd.png')}}" class="img-responsive" alt=""></div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-3">
                        <div class="group">
                            <div class="icon"><img src="{{url('frontend/img/ct.png')}}" class="img-responsive" alt=""></div>
                        </div>
                    </div>
                </div>
              </div>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#home">Home</a></li>
                    <li><a href="#about">About</a></li>
                    <li><a href="#team">Team</a></li>
                    <li><a href="#service">Service</a></li>
                    <li><a href="#client">Our Clients</a></li>
                    <li><a href="#gallery">Gallery</a></li>
                    <li><a href="#contact">Contact us</a></li>
                    <li><a href="#career">Career</a></li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="row nav-serv text-center">
                <div class="col-sm-3 col-xs-6">
                    <div class="group">
                        <div class="icon"><img src="{{url('frontend/img/icon.png')}}" class="img-responsive" alt=""></div>
                        <div class="caption">
                            <div class="txt-dis">WEB
                                <div>Development</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="group">
                        <div class="icon"><img src="{{url('frontend/img/yoyo.png')}}" class="img-responsive" alt=""></div>
                        <div class="caption">
                            <div class="txt-dis">SOFTWARE
                                <div>Development</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="group">
                        <div class="icon"><img src="{{url('frontend/img/hdd.png')}}" class="img-responsive" alt=""></div>
                        <div class="caption">
                            <div class="txt-dis">HARDWARE
                                <div>Maintainance</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="group">
                        <div class="icon"><img src="{{url('frontend/img/ct.png')}}" class="img-responsive" alt=""></div>
                        <div class="caption">
                            <div class="txt-dis">COMPUTER
                                <div>Training</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
