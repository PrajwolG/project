<!-- footer -->
    <footer class="section-padding">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">
            <img src="img/Kitenepal.png" alt="" class="footer-logo">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua.</p>
          </div>
          <div class="col-md-3">
            <h3>Our Services</h3>
            <ul>
              <li><a href="#">Web Development</a></li>
              <li><a href="#">Software Development</a></li>
              <li><a href="#">Hardware Maintenance </a></li>
              <li><a href="#">Computer Training</a></li>
            </ul>
          </div>
          <div class="col-md-3">
            <h3>Pagenav</h3>
            <ul>
              <li><a href="#">Home</a></li>
              <li><a href="#about">About Us</a></li>
              <li><a href="pages/team.html">Team</a></li>
              <li><a href="#service">Our Service</a></li>
              <li><a href="pages/clients.html">Our Clients</a></li>
              <li><a href="pages/gallery.html">Gallery</a></li>
              <li><a href="#contact">Contact</a></li>
              <li><a href="#career">Career</a></li>
            </ul>
          </div>
          <div class="col-md-3">
            <h3>Connect with us</h3>
            <p>
              <a href="https://www.facebook.com/kitenepal/" target="_blank"><i id="social-fb" class="fa fa-facebook-square fa-3x social" target="_blank"></i></a>
              <a href="https://twitter.com/yakakhwo" target="_blank"><i id="social-tw" class="fa fa-twitter-square fa-3x social"></i></a>
              <a href="https://plus.google.com/100415372686005505632" target="_blank"><i id="social-gp" class="fa fa-google-plus-square fa-3x social"></i></a>
              <a href="mailto:info@kitenepal.com"><i id="social-em" class="fa fa-envelope-square fa-3x social"></i></a>
            </p>
            <h3>Linked Company</h3>
            <img src="img/linked.png" alt="" class="img-responsive">
          </div>
        </div>
      </div>
    </footer>
    <div class="copyright">
      <h4 class="text-center">© 2018. All Rights Reserved.</h4>
    </div>
    <!-- /footer -->
    <script type="text/javascript" src="{{url('frontend/js/header.js')}}"></script>
    <script type="text/javascript" src="{{url('frontend/js/owl.js')}}"></script>
    <script type="text/javascript" src="{{url('frontend/js/clients.js')}}"></script>

    <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5b685a79e21878736ba2ab3f/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->