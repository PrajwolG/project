<!--nav-start-->
  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="{{'home'}}"><img src="{{('frontend/img/Kitenepal.png')}}" class="img-responsive" alt=""></a>
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="{{url('')}}">Home</a></li>
          <li><a href="{{url('')}}#about">About</a></li>
          <li><a href="{{url('')}}#team">Team</a></li>
          <li><a href="{{url('')}}#service">Service</a></li>
          <li><a href="{{url('')}}#client">Our Clients</a></li>
          <li><a href="{{url('')}}#gallery">Gallery</a></li>
          <li><a href="{{url('')}}#contact">Contact us</a></li>
          <li><a href="{{url('')}}#career">Career</a></li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- nav end-->