<?php
use App\Classes\Fun;
?>
@extends('front.layout.master')
@section('content')

    <div id="myCarousel" class="carousel slide" data-ride="carousel">
    
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
	    <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>

    <div class="carousel-inner">

      @foreach($slider as $data)
      <div class="@if ($data->active==1) item active @else item @endif">
        <img src="@if($data->status==1){{$data->imagefile}} @endif" alt="Chicago" style="width:100%;">
      </div>
      @endforeach
    </div>
  </div>

    <!-- about us -->
    <section id="about" class="section-padding">
        <div class="container-fluid about">
          <h1 class="heading">{{$about->title}}</h1>
            <div class="row">
                <div class="col-md-9">
                    {!! $about->description !!}
                    <?php 
                    $Desc=strip_tags($about->subdescription);
                    ?>
                    <p id="abt-collapse" class="collapse">
                       {!!$Desc !!}
                    </p>
                        <button class="btn primary-btn" data-toggle="collapse" data-target="#abt-collapse">Read more</button>

                </div>
                <div class="col-md-3">
                    <iframe width="100%" height="200px" src="https://www.youtube.com/embed/UBL9zAChV-E" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    @foreach($gallery as $data)
                  <div class="col-md-3 col-sm-4">
                      <img src="@if($data->category=="About" && $data->status==1){{$data->file}} @endif" alt="" class="img-responsive">
                  </div>
                  @endforeach
                  
                </div>
                <div id="team" class="col-md-3">
                    <div class="carousel-wrap">
                        <div class="owl-carousel">
                            @foreach($team as $data)
                            <div class="item">
                                <a href="{{'team'}}">
                                    <img src="{{$data->file}}">
                                    <div class="extra">
                                        <strong>{{$data->name}}</strong>
                                        <p>{{$data->position}}</p>
                                    </div>
                                </a>
                            </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /about us -->


    <!-- services -->
    <section id="service" class="section-padding services">
        <div class="container-fluid">
            <h1 class="heading">Our Service</h1>
            <div class="row">
                <div class="col-md-9">
                    <ul class="nav nav-tabs">
                        @foreach($service as $data)
                        <li @if($data->active==1) class="active" @else  @endif><a data-toggle="tab" href="#{{$data->id}}">@if($data->status==1){{$data->title}}@endif</a></li>
                        @endforeach
                     </ul>
                    
                    <div class="tab-content">
                        @foreach($service as $data)

                        <div id="{{$data->id}}" class="@if($data->active==1) tab-pane fade in active @else tab-pane fade @endif">
                            <div class="row">
                                
                                @if($data->id==$data->id)
                                
                                <div class="col-md-4">

                                    <img src="{{$data->file}}" alt="" class="img-responsive">
                                </div>
                                <div class="col-md-8">
                                    <h3>{{$data->title}}</h3>
                                    <p>{{strip_tags($data->description)}}</p>
                                    
                                    <a href="#">
                                      <button class="btn primary-btn">View More</button>
                                    </a>

                                </div> 
                                @endif                               
                            </div>
                        </div>
                        @endforeach
                        
                    </div>
                </div>


                <div class="col-md-3">
                    <div id="service-img-slider" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#service-img-slider" data-slide-to="0" class="active"></li>
                            <li data-target="#service-img-slider" data-slide-to="1"></li>
                            <li data-target="#service-img-slider" data-slide-to="2"></li>
                            <li data-target="#service-img-slider" data-slide-to="3"></li>
                        </ol>
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            @foreach($service as $data)
                            <div class="@if($data->active==1)item active @else item @endif">
                                <img src="{{$data->file}}" alt="Web Development" style="width:100%;">
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /services -->

    <!-- clients -->
    <section id="client" class="section-padding">
        <div class="container-fluid">
        	<h1 class="heading">Our Clients</h1>
            <div class="row">
              
                <div class="col-md-9 bhoechie-tab-container clients">
                    <div class="col-sm-3 bhoechie-tab-menu">
                        <div class="list-group">
                          @foreach($group as $data)            
                            <a href="#{{$data->groups}}" class="list-group-item @if($data->active==1) active @endif text-center">
						                  	<i class="fa fa-globe"></i><br/>{{$data->groups}}
						                </a>
                          @endforeach                          
                            
						                <!-- <a href="#" class="list-group-item text-center">
						                  <i class="fa fa-ioxhost"></i><br/>Hosting
						                </a> -->
                        </div>
                    </div>
                    <div class="col-sm-9 bhoechie-tab">
                        <!-- Website section -->
                        <div class="bhoechie-tab-content active">
                            <div class="row">
                            @foreach($Ourclients as $data) 
                            

                                <div class="col-md-4 col-sm-4" id="{{$data->group}}">
                                    <a href="https://alliancelaptoptraining.com.np/" target="_blank">
                                        
                                        <div class="clients--content">
                                            <img src="{{$data->file}}" alt="" class="img-responsive">
                                            <h4>{{$data->title}}</h4>
                                        </div>
                                       
                                    </a>
                                </div>
                            
                             @endforeach
                         

                            <center>
                                <a href="{{'clients'}}">
                                    <button class="btn primary-btn">View More</button>
                                </a>
                            </center>
                        </div>
                        <!-- Website section -->                     
                    </div>

                </div>

              </div>


                <div class="col-md-3">
                	<h3><strong>Testimonial</strong></h3><hr>
                	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              <!-- Wrapper for slides -->
              <div class="carousel-inner">

                @foreach($testimonial as $data)
                <div class="@if($data->active==1) item active @else item @endif">
                  <div class="row" style="padding: 20px">
                    <p class="testimonial_para">{!!$data->description !!}</p><br>
                    <div class="row">
                    <div class="col-sm-4">

                        <img src="{{$data->file}}" class="img-responsive">
                        </div>
                        <div class="col-sm-8">
                        <h4><strong>{{$data->fullname}}</strong></h4>
                        <p class="testimonial_subtitle"><span>{{$data->position}}</span><br>
                        <span>{{$data->category}}</span>
                        </p>
                    </div>
                    </div>
                  </div>
                </div>
                @endforeach


              </div>
            </div>
            <div class="controls testimonial_control pull-right">
                <a class="left fa fa-chevron-left btn btn-default testimonial_btn" href="#carousel-example-generic"
                  data-slide="prev"></a>

                <a class="right fa fa-chevron-right btn btn-default testimonial_btn" href="#carousel-example-generic"
                  data-slide="next"></a>
              </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /clients -->


    <!-- gallery -->
    <section id="gallery" class="section-padding">
    	<div class="container-fluid">
        <h1 class="heading">Gallery</h1>
       <div class="row">
         <div class="col-md-9">
            
            <div class="row">
            @foreach($gallery as $data)
              <div class="col-md-3">
                <div class="gallery--item">
                  <img src="@if($data->category=="Gallery"){{$data->file}} @endif" class="img-responsive">
                  <!-- <div class="overlay">Website</div> -->
                </div>
              </div>
              @endforeach

            </div>
            <center><a href="{{'gallery'}}"><button class="btn primary-btn">View More</button></a></center>
         </div>

         <div class="col-md-3">
           <iframe width="100%" height="200px" src="https://www.youtube.com/embed/UBL9zAChV-E" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
         </div>

       </div>
      </div>
    </section>
    <!-- /gallery -->
    <!-- contact -->
    <section id="contact" class="contact section-padding">
      <div class="container-fluid">
        <h1 class="heading text-center">Contact Us</h1>
        <div class="row">
          <div class="col-md-4">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d220.85189611191444!2d85.42571877982964!3d27.666906544925467!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb1aa646af96dd%3A0x2056c558181268ef!2sKite+Nepal!5e0!3m2!1sen!2snp!4v1529311452697" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
          <div class="col-md-4">
            <h2>KITE Nepal Technosis Pvt. Ltd.</h2>
            <p><i class="fa fa-map-marker"></i> Gapali-04, Suryabinayak, Bhaktapur Bagmati, Nepal.</p>
            <p><i class="fa fa-envelope"></i> info@kitenepal.com</p>
            <p><strong>Support:</strong></p>
            <p><i class="fa fa-envelope"></i> Support@kitenepal.com</p>
            <p><strong>Contact Person:</strong></p>
            <p>Mr. Saroj Suwal</p>
            <p>Managing Director</p>
            <p><i class="fa fa-phone-square"></i> +977-9841131923</p>
          </div>

          <div class="col-md-4">
            <form action="{{url('/ContactUs/store')}}" method="post" enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="form-group">
                <label>Full Name :</label>
                <input type="text" name="fullname" class="form-control">
              </div>
              <div class="form-group">
                <label>Contact No. :</label>
                <input type="text" name="contactno" class="form-control">
              </div>
              <div class="form-group">
                <label>Email :</label>
                <input type="email" name="email" class="form-control">
              </div>
              <div class="form-group">
                <label>Message :</label>
                <textarea class="form-control" rows="6" name="message"></textarea>
              </div>
              <input type="submit" class="btn primary-btn" value="Send" name="">
            </form>
          </div>
        </div>
      </div>
    </section>
    <!-- /contact -->
    <!-- career -->
    <section id="career" class="section-padding">
      <div class="container-fluid">
        <h1 class="text-center heading">Career</h1>
        <div class="row">
          <div class="col-md-4">
            <h3>Vacancy Notice</h3>
            <ol>
              <li>Web Designer</li>
              <li>Software Engineer</li>
              <li>Administrative and Finance Manager</li>
            </ol>
          </div>
          <div class="col-md-8">


            <form action="{{url('/UserCareer/store')}}" method="post" enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" class="form-control" id="name" name="fullname">
              </div>
              <div class="form-group">
                <label for="email">Email address:</label>
                <input type="email" class="form-control" id="email" name="email">
              </div>
              <div class="form-group">
                <label>Submit your CV:</label>
                <input type="file" class="form-control-file" name="cv">
              </div>
              <div class="form-group">
                <label>Submit your recent photo (*pp size):</label>
                <input type="file" class="form-control-file" name="ppimage">
              </div>
              <button type="submit" class="btn primary-btn">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </section>
    <!-- /career -->
    @endsection