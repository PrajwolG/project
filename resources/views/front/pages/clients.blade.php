@extends('front.layout.pagemaster')
@section('page')

<!DOCTYPE html>
<html>


  <!-- nav end-->
    <!-- web clients -->
    <section class="section-padding">
        <div class="container">
            <h1 class="heading">Website's Clients</h1>
            <div class="row">

                @foreach($Ourclients as $data)
                @if($data->group=="Website")
                <div class="col-md-3 col-sm-3">
                    <a href="https://alliancelaptoptraining.com.np/" target="_blank">
                        <div class="clients--content">
                            <img src="{{$data->file}}" alt="" class="img-responsive">
                            <h4>{{$data->title}}</h4>
                        </div>
                    </a>
                </div>
                @endif
                @endforeach


<!--                 <div class="col-md-3 col-sm-3">
                    <a href="http://www.nuwagi.com/" target="_blank">
                        <div class="clients--content">
                            <img src="../img/clients/web/nuwagi.png" alt="" class="img-responsive">
                            <h4>Nuwagi Patrika</h4>
                        </div>
                    </a>
                </div> -->
<!--                 <div class="col-md-3 col-sm-3">
                    <a href="http://www.irdp.org.np/" target="_blank">
                        <div class="clients--content">
                            <img src="../img/clients/web/irdp.png" alt="" class="img-responsive">
                            <h4>IRDP (NGO)</h4>
                        </div>
                    </a>
                </div> -->

<!--                 <div class="col-md-3 col-sm-3">
                    <a href="http://spayurvedic.com/" target="_blank">
                        <div class="clients--content">
                            <img src="../img/clients/web/spa.png" alt="" class="img-responsive">
                            <h4>Ayurvedic Wellness Spa</h4>
                        </div>
                    </a>
                </div> -->
 <!--                <div class="col-md-3 col-sm-3">
                    <a href="http://www.dsunshineresort.com/" target="_blank">
                        <div class="clients--content">
                            <img src="../img/clients/web/sunshine.png" alt="" class="img-responsive">
                            <h4>Sunshine Resort</h4>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-3">
                    <a href="http://www.mountstradacoffee.com/" target="_blank">
                        <div class="clients--content">
                            <img src="../img/clients/web/mount.png" alt="" class="img-responsive">
                            <h4>Mount Strada Coffee Shop</h4>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-3">
                    <a href="#" target="_blank">
                        <div class="clients--content">
                            <img src="../img/clients/web/travel.png" alt="" class="img-responsive">
                            <h4>Trip Travel</h4>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-3">
                    <a href="#" target="_blank">
                        <div class="clients--content">
                            <img src="../img/clients/web/tamang.png" alt="" class="img-responsive">
                            <h4>Hyangla Tamang Samaj</h4>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-3">
                    <a href="http://suprimefund.biz/" target="_blank">
                        <div class="clients--content">
                            <img src="../img/clients/web/suprime.png" alt="" class="img-responsive">
                            <h4>Suprime (Bitcoin)</h4>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-3">
                    <a href="#" target="_blank">
                        <div class="clients--content">
                            <img src="../img/clients/web/arts.png" alt="" class="img-responsive">
                            <h4>Rabita's Arts</h4>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-3">
                    <a href="http://www.bukebaorganiccafe.com.np/" target="_blank">
                        <div class="clients--content">
                            <img src="../img/clients/web/Bukeba.png" alt="" class="img-responsive">
                            <h4>Bukeba Organic Cafe</h4>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-3">
                    <a href="#" target="_blank">
                        <div class="clients--content">
                            <img src="../img/clients/web/mamti.png" alt="" class="img-responsive">
                            <h4>Mamti School</h4>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-3">
                    <a href="http://www.comfortdriving.com.au/" target="_blank">
                        <div class="clients--content">
                            <img src="../img/clients/web/comfort.png" alt="" class="img-responsive">
                            <h4>Comfort Driving School  </h4>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-3">
                    <a href="http://www.citygaon.com/" target="_blank">
                        <div class="clients--content">
                            <img src="../img/clients/web/city.png" alt="" class="img-responsive">
                            <h4>City Gaon Resort</h4>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-3">
                    <a href="http://www.provident.com.np/" target="_blank">
                        <div class="clients--content">
                            <img src="../img/clients/web/provident.png" alt="" class="img-responsive">
                            <h4>Provident Bank</h4>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-3">
                    <a href="http://www.aqualounge.com.np/" target="_blank">
                        <div class="clients--content">
                            <img src="../img/clients/web/aqua.png" alt="" class="img-responsive">
                            <h4>Aqua Resturant</h4>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-3">
                    <a href="#" target="_blank">
                        <div class="clients--content">
                            <img src="../img/clients/web/thanka.png" alt="" class="img-responsive">
                            <h4>Thanka Painting School</h4>
                        </div>
                    </a>
                </div>
  -->           </div>
        </div>
    </section>
    <!-- /web clients -->
    <!-- hosting clients -->


    <section id="hosting" class="section-padding">
        <div class="container">
            <h1 class="heading">Hosting Clients</h1>
            <div class="row">

                @foreach($Ourclients as $data)
                @if($data->group=="Hosting")
                <div class="col-md-3 col-sm-3">
                    <a href="#">
                        <div class="clients--content">
                            <img src="{{$data->file}}" alt="" class="img-responsive">
                            <h4>{{$data->title}}</h4>
                        </div>
                    </a>
                </div>

                @endif
                @endforeach


                <!-- <div class="col-md-3 col-sm-3">
                    <a href="#">
                        <div class="clients--content">
                            <img src="../img/clients/hosting/2.jpg" alt="" class="img-responsive">
                            <h4>Clients Name</h4>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-3">
                    <a href="#">
                        <div class="clients--content">
                            <img src="../img/clients/hosting/3.jpg" alt="" class="img-responsive">
                            <h4>Clients Name</h4>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-3">
                    <a href="#">
                        <div class="clients--content">
                            <img src="../img/clients/hosting/2.jpg" alt="" class="img-responsive">
                            <h4>Clients Name</h4>
                        </div>
                    </a>
                </div> -->


            </div>
        </div>
    </section>
    <!-- /hosting clients -->

    <!-- domain clients -->
    <section id="domain" class="section-padding">
        <div class="container">
            <h1 class="heading">Domain Clients</h1>
            <div class="row">
                @foreach($Ourclients as $data)
                @if($data->group=="Domain")
                <div class="col-md-3 col-sm-3">
                    <a href="#">
                        <div class="clients--content">
                            <img src="{{$data->file}}" alt="" class="img-responsive">
                            <h4>{{$data->title}}</h4>
                        </div>
                    </a>
                </div>
                @endif
                @endforeach

                <!-- 
                <div class="col-md-3 col-sm-3">
                    <a href="#">
                        <div class="clients--content">
                            <img src="../img/clients/domian/2.jpg" alt="" class="img-responsive">
                            <h4>Clients Name</h4>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-3">
                    <a href="#">
                        <div class="clients--content">
                            <img src="../img/clients/domian/1.jpg" alt="" class="img-responsive">
                            <h4>Clients Name</h4>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-3">
                    <a href="#">
                        <div class="clients--content">
                            <img src="../img/clients/domian/2.jpg" alt="" class="img-responsive">
                            <h4>Clients Name</h4>
                        </div>
                    </a>
                </div> -->

            </div>
        </div>
    </section>
    <!-- /domain clients -->

    <!-- hardware clients -->
    <section id="hardware" class="section-padding">
        <div class="container">
            <h1 class="heading">Hardware Clients</h1>
            <div class="row">

                @foreach($Ourclients as $data)
                @if($data->group=="Hardware")
                <div class="col-md-3 col-sm-3">
                    <a href="#">
                        <div class="clients--content">
                            <img src="{{$data->file}}}" alt="" class="img-responsive">
                            <h4>{{$data->title}}</h4>
                        </div>
                    </a>
                </div>

                @endif
                @endforeach

                <!-- <div class="col-md-3 col-sm-3">
                    <a href="#">
                        <div class="clients--content">
                            <img src="../img/clients/hardware/2.jpg" alt="" class="img-responsive">
                            <h4>Clients Name</h4>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-3">
                    <a href="#">
                        <div class="clients--content">
                            <img src="../img/clients/hardware/3.jpg" alt="" class="img-responsive">
                            <h4>Clients Name</h4>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-3">
                    <a href="#">
                        <div class="clients--content">
                            <img src="../img/clients/hardware/1.jpg" alt="" class="img-responsive">
                            <h4>Clients Name</h4>
                        </div>
                    </a>
                </div> -->
            </div>
        </div>
    </section>


    <!-- /hardware clients -->
   
</body>

</html>
@endsection
