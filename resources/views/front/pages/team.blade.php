@extends('front.layout.pagemaster')
@section('page')

<!DOCTYPE html>
<html>
<head>
</head>

<body>

	<!-- team -->
  <section class="section-padding team">
    <div class="container">
      <h1 class="heading">Our Team</h1>

      @foreach($team as $data)
      <div class="row team--member">
        
        <div class="col-md-4">
          <img src="{{$data->file}}" alt="" class="img-responsive img-thumbnail">
        </div>

        <div class="col-md-8">
          <h2>{{$data->name}}</h2>
          <span class="bg-info">{{$data->position}}</span>
          <p>{{strip_tags($data->description)}}</p>
          <p>
              <a href="{{$data->facebook}}" target="_blank"><i id="social-fb" class="fa fa-facebook-square fa-3x social" target="_blank"></i></a>
              <a href="{{$data->twitter}}" target="_blank"><i id="social-tw" class="fa fa-twitter-square fa-3x social"></i></a>
              <a href="{{$data->googlePlus}}"><i id="social-gp" class="fa fa-google-plus-square fa-3x social"></i></a>
              <a href="{{$data->instagram}}"><i id="social-em" class="fa fa-instagram fa-3x social"></i></a>
            </p>
        </div>
        

      </div>

      @endforeach


    </div>
  </section>
  <!-- /team -->
	
		<script type="text/javascript" src="{{url('frontend/js/header.js')}}"></script>
    <script type="text/javascript" src="{{url('js/clients.js')}}"></script>
</body>
</html>

@endsection
