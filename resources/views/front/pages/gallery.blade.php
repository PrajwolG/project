@extends('front.layout.pagemaster')
@section('page')

<!DOCTYPE html>
<html>
<head>
</head>
<body>
  
	<!-- gallery -->
    <section id="gallery" class="section-padding">
        <div class="container">
        <h1 class="heading">Gallery</h1>
       <div class="row">
        @foreach($gallery as $data)
          <div class="col-md-3">
            <div class="gallery--item">
              <img src="{{$data->file}}" class="img-responsive">
              <!-- <div class="overlay">Website</div> -->
            </div>
          </div>
        @endforeach
       </div>
      </div>
    </section>
    <!-- /gallery -->
	
		<script type="text/javascript" src="../js/header.js"></script>
    <script type="text/javascript" src="../js/clients.js"></script>
</body>
</html>

@endsection