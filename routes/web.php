<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    /*return view('front.home');*/
    return redirect()->route('home'); //first priority for home
});

//front

Route::get('/','HomeController@index')->name('home');
Route::get('/home','HomeController@index');
//Route::get('/home','ContactUsController@index')->name('home');
Route::get('/clients','HomeController@clients')->name('clients');
Route::get('/gallery','HomeController@gallery')->name('gallery');
Route::get('/team','HomeController@team')->name('team');






//admin
Route::get('/admin','AdminController@admin')->name('admin');
Route::get('/login','HomeController@login')->name('login');
Route::get('/logout','HomeController@logout')->name('logout');


Auth::routes();


//setting
Route::get('/setting','AdminController@setting')->name('setting');
Route::post('/setting/update/{id}','SettingController@update')->name('update');


//about
Route::get('/about', 'AdminController@about')->name('about');
Route::post('/about/update/{id}', 'AboutController@update')->name('update');



//post
Route::get('/post','AdminController@post')->name('post');

//user
Route::get('/user','AdminController@user')->name('user');
Route::post('/user/store','UserController@store')->name('store');
Route::get('/user/edit/{id}','UserController@edit')->name('user.edit');
Route::post('/user/update/{id}','UserController@update')->name('user.update');
Route::get('/user/delete/{id}','UserController@destroy')->name('user.destroy');




//pages
Route::get('/pages','AdminController@pages')->name('pages');

//slider
Route::get('/slider','AdminController@slider')->name('slider');
Route::post('/slider/store','SlideController@store')->name('store');
Route::get('/slider/edit/{id}','SlideController@edit')->name('slider.edit');
Route::post('/slider/update/{id}','SlideController@update')->name('slider.update');
Route::get('/slider/delete/{id}','SlideController@destroy')->name('slider.destroy');

Route::get('/slider/activate/{id}','SlideController@activate')->name('slider.activate');
Route::get('/slider/inactivate/{id}','SlideController@inactivate')->name('slider.inactivate');


//gallery
Route::get('/D-gallery','AdminController@gallery')->name('D-gallery');
Route::post('/D-gallery/store','GalleryController@store')->name('store');
Route::get('/D-gallery/edit/{id}','GalleryController@edit')->name('D-gallery.edit');
Route::post('/D-gallery/update/{id}','GalleryController@update')->name('D-gallery.update');
Route::get('/D-gallery/delete/{id}','GalleryController@destroy')->name('D-gallery.destroy');
Route::get('/D-gallery/activate/{id}','GalleryController@activate')->name('D-gallery.activate');
Route::get('/D-gallery/inactivate/{id}','GalleryController@inactivate')->name('D-gallery.inactivate');


//team
Route::get('/D-team','AdminController@team')->name('D-team');
Route::post('/D-team/store','TeamController@store')->name('store');
Route::get('/D-team/edit/{id}','TeamController@edit')->name('D-team.edit');
Route::post('/D-team/update/{id}','TeamController@update')->name('D-team.update');
Route::get('/D-team/delete/{id}','TeamController@destroy')->name('D-team.destroy');
Route::get('/D-team/activate/{id}','TeamController@activate')->name('D-team.activate');
Route::get('/D-team/inactivate/{id}','TeamController@inactivate')->name('D-team.inactivate');


//service
Route::get('/service','AdminController@service')->name('service');
Route::get('/viewservice','AdminController@viewservice')->name('viewservice');
Route::post('/service/store','ServiceController@store')->name('store');
Route::get('/service/edit/{id}','ServiceController@edit')->name('service.edit');
Route::post('/service/update/{id}','ServiceController@update')->name('service.update');
Route::get('/service/delete/{id}','ServiceController@destroy')->name('service.destroy');

Route::get('/service/activate/{id}','ServiceController@activate')->name('service.activate');
Route::get('/service/inactivate/{id}','ServiceController@inactivate')->name('service.inactivate');



//ourclients
Route::get('/ourclients','AdminController@ourclients')->name('ourclients');
Route::get('/viewclients','AdminController@viewclients')->name('viewclients');
Route::post('/ourclients/store','OurClientsController@store')->name('store');
Route::get('/ourclients/edit/{id}','OurClientsController@edit')->name('ourclients.edit');
Route::post('/ourclients/update/{id}','OurClientsController@update')->name('ourclients.update');
Route::get('/ourclients/delete/{id}','OurClientsController@destroy')->name('ourclients.destroy');

Route::get('/ourclients/activate/{id}','OurClientsController@activate')->name('ourclients.activate');
Route::get('/ourclients/inactivate/{id}','OurClientsController@inactivate')->name('ourclients.inactivate');


//career
Route::get('/career','AdminController@career')->name('career');
Route::post('/career/store','CareerController@store')->name('store');


//testimonial
Route::get('/testimonial','AdminController@testimonial')->name('testimonial');
Route::post('/testimonial/store','TestimonialController@store')->name('store');
Route::get('/testimonial/edit/{id}','TestimonialController@edit')->name('testimonial.edit');
Route::post('/testimonial/update/{id}','TestimonialController@update')->name('testimonial.update');
Route::get('/testimonial/delete/{id}','TestimonialController@destroy')->name('testimonial.destroy');
Route::get('/testimonial/activate/{id}','TestimonialController@activate')->name('testimonial.activate');
Route::get('/testimonial/inactivate/{id}','TestimonialController@inactivate')->name('testimonial.inactivate');

//User Career
Route::get('/UserCareer','AdminController@UserCareer')->name('UserCareer');
Route::post('/UserCareer/store','UserCareerController@store')->name('store');
Route::get('/UserCareer/delete/{id}','UserCareerController@destroy')->name('UserCareer.destroy');


//ContactUs
Route::get('/ContactUs','AdminController@ContactUs')->name('ContactUs');
Route::post('/ContactUs/store','ContactUsController@store')->name('store');
Route::get('/ContactUs/delete/{id}','ContactUsController@destroy')->name('ContactUs.destroy');


