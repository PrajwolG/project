-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 03, 2018 at 03:41 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kitenepal`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `subdescription` longtext COLLATE utf8mb4_unicode_ci,
  `caption` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaTag` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaDescription` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `title`, `description`, `subdescription`, `caption`, `keywords`, `metaTag`, `metaDescription`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'a truly IT Company', '<p>KITE Nepal&nbsp;Technosis Pvt Ltd . is one of the IT companies in Nepal registered under company&#39;s Act 2063. The main objective of the company is to provide IT related services. Here we provide many services like web development, software development and sales, chip level maintenance and repairing, computer training.</p>\r\n\r\n<p>We also provide job&nbsp; placement facilities to those people who have relvant skiils. We train and place intern to earn experience and make capabale to work PHP on Laravel framework, JAVA, dot net, mobile apps development is main features of the company&nbsp;</p>', '<p>Provident Marchant Banking Ltd. SEBON Licence no. 33 with Agency no &quot;A009&quot; is also special features for Bhaktapur city where share investors in Bhaktapur can get easily services and trainig about share.</p>\r\n\r\n<p>&nbsp;</p>', 'About', NULL, NULL, NULL, 4, NULL, '2018-08-03 07:19:23');

-- --------------------------------------------------------

--
-- Table structure for table `careers`
--

CREATE TABLE `careers` (
  `id` int(10) UNSIGNED NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filetype` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `caption` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaTag` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaDescription` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `careers`
--

INSERT INTO `careers` (`id`, `file`, `filetype`, `description`, `caption`, `keywords`, `metaTag`, `metaDescription`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'img/career/5b5ac3a445313-bg-1.jpg', 'image', '<p>Description</p>', NULL, NULL, NULL, NULL, 4, '2018-07-27 01:18:00', '2018-07-27 01:18:00');

-- --------------------------------------------------------

--
-- Table structure for table `client_groups`
--

CREATE TABLE `client_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `groups` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `client_groups`
--

INSERT INTO `client_groups` (`id`, `groups`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Website', 1, NULL, NULL),
(2, 'Hosting', 0, NULL, NULL),
(3, 'Domain', 0, NULL, NULL),
(4, 'Hardware', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contactuses`
--

CREATE TABLE `contactuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactno` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contactuses`
--

INSERT INTO `contactuses` (`id`, `name`, `contactno`, `email`, `message`, `created_at`, `updated_at`) VALUES
(3, 'test', 'test', 'test@gmail.com', 'test', '2018-07-31 03:56:02', '2018-07-31 03:56:02');

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` int(10) UNSIGNED NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  `caption` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaTag` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaDescription` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `file`, `category`, `link`, `status`, `rank`, `caption`, `keywords`, `metaTag`, `metaDescription`, `user_id`, `created_at`, `updated_at`) VALUES
(10, 'img/gallery/5b5b798f577e6-abt1.jpg', 'About', 'abt1.jpg', 1, 1, NULL, NULL, NULL, NULL, 4, '2018-07-27 14:14:11', '2018-08-01 04:02:51'),
(11, 'img/gallery/5b5b7995e3c72-abt2.jpg', 'About', 'abt2.jpg', 1, 1, NULL, NULL, NULL, NULL, 4, '2018-07-27 14:14:17', '2018-08-01 04:02:55'),
(12, 'img/gallery/5b5b799fb8f49-abt3.jpg', 'About', 'abt3.jpg', 1, 1, NULL, NULL, NULL, NULL, 4, '2018-07-27 14:14:27', '2018-08-01 03:49:45'),
(13, 'img/gallery/5b5b79b77bd4e-abt4.jpg', 'About', 'abt4.jpg', 1, 1, NULL, NULL, NULL, NULL, 4, '2018-07-27 14:14:51', '2018-08-01 03:49:49'),
(14, 'img/gallery/5b5b79be35d75-abt5.jpg', 'About', 'abt5.jpg', 1, 1, NULL, NULL, NULL, NULL, 4, '2018-07-27 14:14:58', '2018-08-01 03:49:54'),
(15, 'img/gallery/5b5b79c6e6950-abt6.jpg', 'About', 'abt6.jpg', 1, 1, NULL, NULL, NULL, NULL, 4, '2018-07-27 14:15:07', '2018-08-01 03:49:58'),
(16, 'img/gallery/5b5b79d4b6b02-abt7.jpg', 'About', 'abt7.jpg', 1, 1, NULL, NULL, NULL, NULL, 4, '2018-07-27 14:15:20', '2018-08-01 03:50:17'),
(17, 'img/gallery/5b5b79dc2d3ad-abt8.jpg', 'About', 'abt8.jpg', 1, 1, NULL, NULL, NULL, NULL, 4, '2018-07-27 14:15:28', '2018-08-01 03:50:21'),
(18, 'img/gallery/5b5bcfd1aba73-web.jpg', 'Gallery', 'web.jpg', 1, 1, NULL, NULL, NULL, NULL, 4, '2018-07-27 20:22:14', '2018-08-01 04:03:21'),
(19, 'img/gallery/5b5bd39369047-software.jpg', 'Gallery', 'software.jpg', 1, 1, NULL, NULL, NULL, NULL, 4, '2018-07-27 20:38:15', '2018-08-01 04:03:05'),
(20, 'img/gallery/5b5bd3a5dd3cf-hardware.jpg', 'Gallery', 'hardware.jpg', 1, 1, NULL, NULL, NULL, NULL, 4, '2018-07-27 20:38:33', '2018-08-01 04:03:13'),
(21, 'img/gallery/5b5bd3af94238-class.jpg', 'Gallery', 'class.jpg', 0, 1, NULL, NULL, NULL, NULL, 4, '2018-07-27 20:38:43', '2018-07-27 20:38:43'),
(22, 'img/gallery/5b5f04c921877-web.jpg', 'Gallery', 'web.jpg', 0, 1, NULL, NULL, NULL, NULL, 4, '2018-07-30 06:45:01', '2018-07-30 06:45:01');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2014_10_12_000000_create_users_table', 1),
(5, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2018_07_12_165802_create_sliders_table', 1),
(7, '2018_07_12_170851_create_settings_table', 2),
(8, '2018_07_12_171130_create_abouts_table', 3),
(10, '2018_07_13_094559_create_galleries_table', 4),
(11, '2018_07_26_105134_create_teams_table', 5),
(12, '2018_07_27_000136_create_services_table', 6),
(13, '2018_07_27_001145_create_ourclients_table', 7),
(14, '2018_07_27_001817_create_ourclientsextras_table', 8),
(15, '2018_07_27_002818_create_careers_table', 9),
(16, '2018_07_27_121310_create_testimonials_table', 10),
(17, '2018_07_29_193617_create_contact_uses_table', 11),
(19, '2018_07_29_193946_create_user_careers_table', 12),
(20, '2018_07_31_123925_create_client_groups_table', 13);

-- --------------------------------------------------------

--
-- Table structure for table `ourclients`
--

CREATE TABLE `ourclients` (
  `id` int(10) UNSIGNED NOT NULL,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` longtext COLLATE utf8mb4_unicode_ci,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rank` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `caption` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaTag` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaDescription` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ourclients`
--

INSERT INTO `ourclients` (`id`, `group`, `file`, `title`, `link`, `status`, `rank`, `caption`, `keywords`, `metaTag`, `metaDescription`, `user_id`, `created_at`, `updated_at`) VALUES
(15, '1', 'img/ourclients/5b5bdf57320b8-alliance.png', 'Alliance Pvt. Ltd', 'www.google.com', '0', '1', NULL, NULL, NULL, NULL, 4, '2018-07-27 21:28:27', '2018-08-01 03:40:09'),
(16, '1', 'img/ourclients/5b5bdf741c58f-nuwagi.png', 'Nuwagi Patrika', 'www.google.com', '0', '1', NULL, NULL, NULL, NULL, 4, '2018-07-27 21:28:56', '2018-07-27 21:28:56'),
(17, '1', 'img/ourclients/5b5bdf8295430-irdp.png', 'IRDP (NGO)', 'www.google.com', '0', '1', NULL, NULL, NULL, NULL, 4, '2018-07-27 21:29:10', '2018-07-27 21:29:10'),
(18, '2', 'img/ourclients/5b5be0104d7be-1.jpg', 'Code Host', 'www.google.com', '0', '1', NULL, NULL, NULL, NULL, 4, '2018-07-27 21:31:32', '2018-07-27 21:31:32'),
(19, '2', 'img/ourclients/5b5be01fe1c64-2.jpg', 'Hosting', 'www.google.com', '0', '1', NULL, NULL, NULL, NULL, 4, '2018-07-27 21:31:47', '2018-07-27 21:31:47'),
(20, '2', 'img/ourclients/5b5be030ab2ed-3.jpg', 'Hosting', 'www.google.com', '0', '1', NULL, NULL, NULL, NULL, 4, '2018-07-27 21:32:04', '2018-07-27 21:32:04'),
(21, '3', 'img/ourclients/5b5be041d8867-1.jpg', 'WWW', 'www.google.com', '0', '1', NULL, NULL, NULL, NULL, 4, '2018-07-27 21:32:21', '2018-07-27 21:32:21'),
(22, '3', 'img/ourclients/5b5be0507339a-2.jpg', 'WWW', 'www.google.com', '0', '1', NULL, NULL, NULL, NULL, 4, '2018-07-27 21:32:36', '2018-07-27 21:32:36'),
(23, '3', 'img/ourclients/5b5be05cd8f5c-1.jpg', 'WWW', 'www.google.com', '0', '1', NULL, NULL, NULL, NULL, 4, '2018-07-27 21:32:48', '2018-07-27 21:32:48'),
(24, '4', 'img/ourclients/5b5be0720f17e-1.jpg', 'Hardware', 'www.google.com', '0', '1', NULL, NULL, NULL, NULL, 4, '2018-07-27 21:33:10', '2018-07-27 21:33:10'),
(25, '4', 'img/ourclients/5b5be0804a908-2.jpg', 'Hardware', 'www.google.com', '0', '1', NULL, NULL, NULL, NULL, 4, '2018-07-27 21:33:24', '2018-07-27 21:33:24'),
(26, '4', 'img/ourclients/5b5be091daca0-3.jpg', 'Hardware', 'www.google.com', '0', '1', NULL, NULL, NULL, NULL, 4, '2018-07-27 21:33:41', '2018-07-27 21:33:41'),
(29, '3', 'img/ourclients/5b6422f791fb9-slider1.jpg', 'WWW', 'abc', '0', '1', 'abc', NULL, NULL, NULL, 4, '2018-08-03 03:55:07', '2018-08-03 03:55:30');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `extraDescription` longtext COLLATE utf8mb4_unicode_ci,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` longtext COLLATE utf8mb4_unicode_ci,
  `active` int(11) DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rank` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `caption` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaTag` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaDescription` longtext COLLATE utf8mb4_unicode_ci,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `title`, `description`, `extraDescription`, `file`, `link`, `active`, `status`, `rank`, `caption`, `keywords`, `metaTag`, `metaDescription`, `user_id`, `created_at`, `updated_at`) VALUES
(3, 'Web Development', '<p><span style=\"font-family: &quot;Roboto Slab&quot;, serif; font-size: 16px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>', '<p><span style=\"font-family: &quot;Roboto Slab&quot;, serif; font-size: 16px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>', 'img/service/5b5b69b2cb2a8-web.jpg', NULL, 1, '1', '1', NULL, NULL, NULL, NULL, 4, '2018-07-27 13:06:31', '2018-08-03 03:12:36'),
(4, 'Software Development', '<p><span style=\"font-family: &quot;Roboto Slab&quot;, serif; font-size: 16px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>', '<p><span style=\"font-family: &quot;Roboto Slab&quot;, serif; font-size: 16px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>', 'img/service/5b5b6a81c9dae-software.jpg', NULL, 0, '1', '1', NULL, NULL, NULL, NULL, 4, '2018-07-27 13:09:57', '2018-08-03 03:11:37'),
(5, 'Harware Maintenance', '<p><span style=\"font-family: &quot;Roboto Slab&quot;, serif; font-size: 16px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>', '<p><span style=\"font-family: &quot;Roboto Slab&quot;, serif; font-size: 16px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>', 'img/service/5b5b6aa47d793-hardware.jpg', NULL, 0, '1', '1', NULL, NULL, NULL, NULL, 4, '2018-07-27 13:10:32', '2018-08-03 03:12:38'),
(6, 'Computer Training', '<p><span style=\"font-family: &quot;Roboto Slab&quot;, serif; font-size: 16px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>', '<p><span style=\"font-family: &quot;Roboto Slab&quot;, serif; font-size: 16px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>', 'img/service/5b5b6ab89e531-class.jpg', NULL, 0, '1', '1', NULL, NULL, NULL, NULL, 4, '2018-07-27 13:10:52', '2018-08-03 04:05:00');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_map` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_plus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `caption` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaTag` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaDescription` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `company`, `logo`, `address`, `contact`, `email`, `google_map`, `facebook`, `youtube`, `google_plus`, `twitter`, `instagram`, `caption`, `keywords`, `metaTag`, `metaDescription`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Kite Nepal', 'img/setting/5b48924b1d393-favicon.png', 'Bhaktapur', '9843398399', 'kitenepal@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, 'Kite Nepal', NULL, NULL, NULL, 1, NULL, '2018-07-13 06:06:39');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `imagefile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` longtext COLLATE utf8mb4_unicode_ci,
  `active` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `rank` int(11) NOT NULL,
  `caption` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaTag` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaDescription` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `imagefile`, `link`, `active`, `status`, `rank`, `caption`, `keywords`, `metaTag`, `metaDescription`, `user_id`, `created_at`, `updated_at`) VALUES
(19, 'img/slider/5b6408dce9b48-slider1.jpg', 'slider1.jpg', 1, 1, 1, NULL, NULL, NULL, NULL, 4, '2018-07-27 08:29:19', '2018-08-03 02:44:25'),
(20, 'img/slider/5b5b28c627060-slider2.jpg', 'slider2.jpg', 0, 1, 1, NULL, NULL, NULL, NULL, 4, '2018-07-27 08:29:30', '2018-08-01 03:54:05'),
(21, 'img/slider/5b5b28ceec17b-slider3.jpg', 'slider3.jpg', 0, 1, 1, NULL, NULL, NULL, NULL, 4, '2018-07-27 08:29:39', '2018-08-01 03:54:07'),
(22, 'img/slider/5b5b28da375da-slider4.jpg', 'slider4.jpg', 0, 1, 1, NULL, NULL, NULL, NULL, 4, '2018-07-27 08:29:50', '2018-08-01 03:57:18');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `facebook` longtext COLLATE utf8mb4_unicode_ci,
  `twitter` longtext COLLATE utf8mb4_unicode_ci,
  `googlePlus` longtext COLLATE utf8mb4_unicode_ci,
  `instagram` longtext COLLATE utf8mb4_unicode_ci,
  `status` int(11) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  `caption` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaTag` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaDescription` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `name`, `file`, `position`, `description`, `facebook`, `twitter`, `googlePlus`, `instagram`, `status`, `rank`, `caption`, `keywords`, `metaTag`, `metaDescription`, `user_id`, `created_at`, `updated_at`) VALUES
(8, 'Saroj Suwal', 'img/team/5b5b67197aec8-saroj.jpg', 'Managing Director', '<p><span style=\"color: rgb(63, 20, 91); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">More than 10 years of experience on IT field. Have good knowledge of IT counseling and IT Marketing. Excellent software architecture designing, software development, analysis and web development. Engineering on Software Development is major faculty and good lecturer at computer training like, Chip Level Maintenance, Advance PHP, JAVA, MYSQL, SQL 2008, Visual Studio 2010, Certified Public Accountant, AutoCAD and many more.</span></p>', 'https://www.facebook.com/yakakhwo', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, 4, '2018-07-27 12:55:26', '2018-08-03 07:05:27'),
(9, 'Suraj Kapali', 'img/team/5b5b6753e5f84-suraj.jpg', 'Front End Developer', '<p><span style=\"color: rgb(63, 20, 91); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">+2 in Computer Science from Shree Padma Higher. Advance PHP from CITS (College of IT Sansar).</span></p>', NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, 4, '2018-07-27 12:56:24', '2018-08-03 04:09:46'),
(10, 'Nidan Shrestha', 'img/team/5b5b67ad373a8-nidan.jpg', 'Front End Developer', '<p><span style=\"color: rgb(63, 20, 91); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">+2 in Computer Science from Supreme Acadamy. Advance PHP from CITS (College of IT Sansar).</span></p>', NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, 4, '2018-07-27 12:57:53', '2018-08-03 06:12:16'),
(11, 'Sabina Maka', 'img/team/5b5b683640647-sabina.jpg', 'Designer', '<p><span style=\"color: rgb(63, 20, 91); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Bachelor in Business Studies from Khwopa College. Advance PHP from CITS (College of IT Sansar).</span></p>', NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, 4, '2018-07-27 13:00:10', '2018-08-03 07:21:25'),
(12, 'Sapana Sunuwar', 'img/team/5b5b685386331-sapana.jpg', 'Accountant', '<p><span style=\"color: rgb(63, 20, 91); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Bachelor in Business Studies from Khwopa College.</span></p>', 'fb.com', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, 4, '2018-07-27 13:00:39', '2018-08-03 07:21:50');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(10) UNSIGNED NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fullname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `active` int(191) DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rank` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `caption` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaTag` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaDescription` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `file`, `fullname`, `position`, `category`, `description`, `active`, `status`, `rank`, `caption`, `keywords`, `metaTag`, `metaDescription`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'img/testimonial/5b5b191b6a762-avatar1.jpg', 'Avatar', 'Head', 'Computer Training', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 1, '1', '1', NULL, NULL, NULL, NULL, 4, '2018-07-27 07:22:39', '2018-08-03 04:10:37'),
(3, 'img/testimonial/5b5b20f497687-profile-avatar.jpg', 'Avatar2', 'Head', 'Student', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 0, '1', '1', NULL, NULL, NULL, NULL, 4, '2018-07-27 07:56:08', '2018-08-03 04:10:39'),
(5, 'img/testimonial/5b5efe4c0ce2c-3.jpg', 'new1', 'new', 'Web Development', '<p>new</p>', NULL, '1', '1', NULL, NULL, NULL, NULL, 4, '2018-07-30 06:17:21', '2018-08-03 04:14:49');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(4, 'Prajwol', 'Prajwolghemosu399@gmail.com', '$2y$10$7OechI8rIGMyBy3W7v9faem4aVWCO7ScfqFz9eKSP1tbqevBS4NS6', 'LgC55oVV3cHO28vrZdyAGXm4t9FtlaPCeNdVgxWXJTLSPQdP5QblzgXv1GCi', '2018-07-25 06:59:13', '2018-07-25 06:59:13'),
(8, 'Prazol', 'Prazol@gmail.com', '$2y$10$c9LuHLql.OO5Am6pEIG6d.ZFhvamveVaff5XFVTTKSika9OqE/5aS', NULL, '2018-07-27 02:13:05', '2018-07-27 02:13:05');

-- --------------------------------------------------------

--
-- Table structure for table `user_careers`
--

CREATE TABLE `user_careers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cv` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ppimage` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_careers`
--

INSERT INTO `user_careers` (`id`, `name`, `email`, `cv`, `ppimage`, `created_at`, `updated_at`) VALUES
(1, 'Prazol', 'Prajwolghemosu399@gmail.com', 'C:\\xampp\\tmp\\phpA4F6.tmp', 'img/ppimage/5b60b05a9d30a-software.jpg', '2018-07-31 13:09:19', '2018-07-31 13:09:19'),
(2, 'new', 'new@gmail.com', 'C:\\xampp\\tmp\\php3B7D.tmp', 'img/ppimage/5b60b14596cb3-class.jpg', '2018-07-31 13:13:13', '2018-07-31 13:13:13'),
(3, 'Prazol', 'P@gmail.com', 'C:\\xampp\\tmp\\php3B56.tmp', 'img/ppimage/5b60b1c892462-software.jpg', '2018-07-31 13:15:24', '2018-07-31 13:15:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careers`
--
ALTER TABLE `careers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_groups`
--
ALTER TABLE `client_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contactuses`
--
ALTER TABLE `contactuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ourclients`
--
ALTER TABLE `ourclients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_careers`
--
ALTER TABLE `user_careers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `careers`
--
ALTER TABLE `careers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `client_groups`
--
ALTER TABLE `client_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `contactuses`
--
ALTER TABLE `contactuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `ourclients`
--
ALTER TABLE `ourclients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_careers`
--
ALTER TABLE `user_careers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
