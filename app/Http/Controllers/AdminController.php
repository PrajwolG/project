<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Setting;
use App\About;
use App\Slider;
use App\Gallery;
use App\Team;
use App\Service;
use App\Ourclients;
use App\Career;
use App\Testimonial;
use App\UserCareer;
use App\ContactUs;


class AdminController extends Controller
{
    //
        public function __construct()
    {
        $this->middleware('auth');
    }

    public function admin()
    {
    	$setting=setting::all();
    
        return view('back.admin',['row'=>$setting]);
    }

        public function about()
    {
        $about=About::all();
        $setting=setting::all();

        return view('back.about.index',['rows'=>$about],['row'=>$setting]);
    }

/*    public function login()
    {
    	return view('back.pages.login');
    }*/

    public function pages()
    {
        $setting=setting::all();
 

    	return view('back.pages.pages',['row'=>$setting]);
    
        
    }

  public function post()
    {
        $setting=setting::all();
    	return view('back.post.index',['row'=>$setting]);
    }
     
      public function gallery()
    {

        $setting=setting::all();
        $gallery=Gallery::paginate(9);
        
    	return view('back.gallery.create',['row'=>$setting,'gallery'=>$gallery]);
    }

          
        public function user()
    {
        $user=User::paginate(10);

        $setting=setting::all();
    	return view('back.user.create',['rows'=>$user,'row'=>$setting]);

    }

     

        public function slider()
    {
        $setting=Setting::all();
        $slider=Slider::paginate(9);
        //dd($sl);
        return view('back.slider.create',['row'=>$setting,'slider'=>$slider]);
    }




    
     public function setting()
    {
    	$setting=setting::all();
        return view('back.setting.index',['rows'=>$setting],['row'=>$setting]);
    }

        public function team()
    {
        $setting=Setting::all();
        $team=Team::paginate(9);
        //dd($sl);
        return view('back.team.create',['row'=>$setting,'team'=>$team]);
    }


        public function service()
    {
        $setting=Setting::all();
        $service=Service::all();
        //dd($sl);
        return view('back.service.index',['row'=>$setting,'service'=>$service]);
    }


        public function viewservice()
    {
        $setting=Setting::all();
        $service=Service::paginate(10);
        //dd($sl);
        return view('back.service.view',['row'=>$setting,'service'=>$service]);
    }




        public function ourclients()
    {
        $setting=Setting::all();
        $ourclients=Ourclients::all();
        //dd($sl);
        return view('back.ourclients.index',['row'=>$setting,'ourclients'=>$ourclients]);
    }

        public function viewclients()
    {
        $setting=Setting::all();
        $ourclients=Ourclients::paginate(10);
        //dd($sl);
        return view('back.ourclients.view',['row'=>$setting,'ourclients'=>$ourclients]);
    }


        public function career()
    {
        $setting=Setting::all();
        $career=Career::all();
        //dd($sl);
        return view('back.career.index',['row'=>$setting,'career'=>$career]);
    }


        public function testimonial()
    {
        $setting=Setting::all();
        $testimonial=Testimonial::paginate(10);
        //dd($sl);
        return view('back.testimonial.create',['row'=>$setting,'testimonial'=>$testimonial]);
    }

        public function UserCareer()
    {
        $setting=Setting::all();
        $UserCareer=UserCareer::paginate(10);
        //dd($sl);
        return view('back.usercareer.index',['row'=>$setting,'UserCareer'=>$UserCareer]);
    }

        public function ContactUs()
    {
        $setting=Setting::all();
        $ContactUs=ContactUs::paginate(10);
        //dd($sl);
        return view('back.contactus.index',['row'=>$setting,'ContactUs'=>$ContactUs]);
    }






}
