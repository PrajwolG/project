<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;
use App\Slider;
use Auth;
use Input as Input;
use File;
use Image;
use App\Team;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $team=new Team();

        $img=$request->newfile;
        //newly added

        if(!is_null($img))
        { 
            $dest_path = "img/team/";
            $filename =uniqid()."-".$img->getClientOriginalName();
            
            //resize image
            $img = Image::make($img->getRealPath());
            $img->resize(400, 400);
            $img->save(public_path($dest_path.$filename));

            $image = $dest_path.$filename;
            
            $team->file=$image;
                
        }

            else
    {
        
    
        return redirect('D-team');

    }

        $team->name=$request->fullname;
        $team->position=$request->position;
        $team->description=$request->description;
        $team->facebook=$request->facebook;
        $team->twitter=$request->twitter;
        $team->googlePlus=$request->googlePlus;
        $team->instagram=$request->instagram;

        $team->status=0;
        $team->rank=1;
        $team->caption=$request->caption;
        $team->keywords=$request->keywords;
        $team->metaTag=$request->metaTag;
        $team->metaDescription=$request->metaDescription;
        $team->user_id=Auth::id();
        $team->save();
        return redirect('D-team');






    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $setting=Setting::all();
        $team=Team::paginate(9);
        $result=Team::find($id);
        //dd($sl);
        return view('back.team.edit',['row'=>$setting,'team'=>$team,'result'=>$result]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $team=Team::find($id);

        $img=$request->file;
        //newly added

        if(!is_null($img))
        { 
            $file_path= $request->image;
            
            if(file_exists($file_path))
            {
            
            unlink($file_path);
            $dest_path = "img/team/";
            $filename =uniqid()."-".$img->getClientOriginalName();
            
            //resize image
            $img = Image::make($img->getRealPath());
            $img->resize(400, 400);
            $img->save(public_path($dest_path.$filename));

            $team->file = $dest_path.$filename;
            
            //$team->imagefile=$image;
            
            }

            else
            {
            
            $dest_path = "img/team/";
            $filename =uniqid()."-".$img->getClientOriginalName();
            
            //resize image
            $img = Image::make($img->getRealPath());
            $img->resize(400, 400);
            $img->save(public_path($dest_path.$filename));

            $team->file = $dest_path.$filename;
            
            //$slider->imagefile=$image;
            
            }

                
        }
        else
        {
            $team->file=$request->image;

        }

        $team->name=$request->fullname;
        $team->position=$request->position;
        $team->description=$request->description;
        $team->facebook=$request->facebook;
        $team->twitter=$request->twitter;
        $team->googlePlus=$request->googlePlus;
        $team->instagram=$request->instagram;
        $team->status=0;
        $team->rank=1;
        $team->caption=$request->caption;
        $team->keywords=$request->keywords;
        $team->metaTag=$request->metaTag;
        $team->metaDescription=$request->metaDescription;
        $team->user_id=Auth::id();
        $team->save();
        return redirect('D-team');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $data=Team::find($id);
        if(!is_null($data->file)) //image files
        {
            $file_path=$data->file;
            if(file_exists($file_path))
                unlink($file_path);
        }
        
        $data->delete();
        return redirect('D-team');


    }


     public function inactivate(Request $request, $id)
    {
        $team=Team::find($id);
        $team->user_id=Auth::id();
        $team->status=0;

        $team->save();
        return redirect('D-team'); 
    }


    public function activate(Request $request, $id)
    {
        $team=Team::find($id);
        $team->user_id=Auth::id();
        $team->status=1;
        //dd($slider);

        $team->save();
        return redirect('D-team'); 
    }
}
