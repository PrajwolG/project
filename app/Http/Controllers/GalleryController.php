<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;
use App\Gallery;
use Auth;
use Input as Input;
use File;
use Image; 



class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $gallery=new Gallery();

        $img=$request->file;
        //newly added

        if(!is_null($img))
        { 
            $dest_path = "img/gallery/";
            $filename =uniqid()."-".$img->getClientOriginalName();
            
            //resize image
            $img = Image::make($img->getRealPath());
            /*$img->resize(500, 400);*/
            $img->save(public_path($dest_path.$filename));

            $image = $dest_path.$filename;
            
            $gallery->file=$image;
                
        }

            else
    {
        
    
        return redirect('D-gallery');

    }


        $gallery->link=$request->link;
        $gallery->category=$request->category;

        $gallery->status=0;
        $gallery->rank=1;
        $gallery->caption=$request->caption;
        $gallery->keywords=$request->keywords;
        $gallery->metaTag=$request->metaTag;
        $gallery->metaDescription=$request->metaDescription;
        $gallery->user_id=Auth::id();
        $gallery->save();
        return redirect('D-gallery');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $setting=Setting::all();
        $gallery=Gallery::paginate(9);
        $result=Gallery::find($id);
        //dd($sl);
        return view('back.gallery.edit',['row'=>$setting,'gallery'=>$gallery,'result'=>$result]);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $gallery=Gallery::find($id);

        $img=$request->file;
        //newly added

        if(!is_null($img))
        { 
            $file_path= $request->image;
            
            if(file_exists($file_path))
            {
            
            unlink($file_path);
            $dest_path = "img/gallery/";
            $filename =uniqid()."-".$img->getClientOriginalName();
            
            //resize image
            $img = Image::make($img->getRealPath());
            $img->resize(500, 400);
            $img->save(public_path($dest_path.$filename));

            $image = $dest_path.$filename;
            
            $gallery->file=$image;
            
            }

            else
            {
            
            $dest_path = "img/gallery/";
            $filename =uniqid()."-".$img->getClientOriginalName();
            
            //resize image
            $img = Image::make($img->getRealPath());
            $img->resize(500, 400);
            $img->save(public_path($dest_path.$filename));

            $image = $dest_path.$filename;
            
            $gallery->file=$image;
            
            }

                
        }
        else
        {
            $gallery->file=$request->image;

        }

        $gallery->link=$request->link;
        $gallery->category=$request->category;
        $gallery->status=0;
        $gallery->rank=1;
        $gallery->caption=$request->caption;
        $gallery->keywords=$request->keywords;
        $gallery->metaTag=$request->metaTag;
        $gallery->metaDescription=$request->metaDescription;
        $gallery->user_id=Auth::id();

        $gallery->save();
        return redirect('D-gallery');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //


        $data=Gallery::find($id);
        if(!is_null($data->file)) //image files
        {
            $file_path=$data->file;
            if(file_exists($file_path))
                unlink($file_path);
        }
        
        $data->delete();
        return redirect('D-gallery');


    }


     public function inactivate(Request $request, $id)
    {
        $gallery=Gallery::find($id);
        $gallery->user_id=Auth::id();
        $gallery->status=0;

        $gallery->save();
        return redirect('D-gallery'); 
    }


    public function activate(Request $request, $id)
    {
        $gallery=Gallery::find($id);
        $gallery->user_id=Auth::id();
        $gallery->status=1;
        //dd($gallery);

        $gallery->save();
        return redirect('D-gallery'); 
    }
}
