<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ourclients;
use App\Setting;
use Auth;
use Input as Input;
use File;
use Image;

class OurClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $Ourclients=new Ourclients();

        $img=$request->file;
        //newly added

        if(!is_null($img))
        { 
            $dest_path = "img/ourclients/";
            $filename =uniqid()."-".$img->getClientOriginalName();
            
            //resize image
            $img = Image::make($img->getRealPath());
            $img->resize(400,400);
            $img->save(public_path($dest_path.$filename));

            $image = $dest_path.$filename;
            
            $Ourclients->file=$image;
                
        }

            else
    {
        
    
        return redirect('ourclients');

    }

        $Ourclients->group=$request->group;
        $Ourclients->title=$request->title;
        $Ourclients->link=$request->link;
        $Ourclients->status=0;
        $Ourclients->rank=1;
        $Ourclients->caption=$request->caption;
        $Ourclients->keywords=$request->keywords;
        $Ourclients->metaTag=$request->metaTag;
        $Ourclients->metaDescription=$request->metaDescription;
        $Ourclients->user_id=Auth::id();
        $Ourclients->save();
        return redirect('ourclients');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $setting=Setting::all();
        $Ourclients=Ourclients::paginate(9);
        $result=Ourclients::find($id);
        //dd($sl);
        return view('back.ourclients.edit',['row'=>$setting,'Ourclients'=>$Ourclients,'result'=>$result]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $Ourclients=Ourclients::find($id);

        $img=$request->file;
        //newly added

        if(!is_null($img))
        { 
            $file_path= $request->image;
            
            if(file_exists($file_path))
            {
            
            unlink($file_path);
            $dest_path = "img/Ourclients/";
            $filename =uniqid()."-".$img->getClientOriginalName();
            
            //resize image
            $img = Image::make($img->getRealPath());
            $img->resize(400, 400);
            $img->save(public_path($dest_path.$filename));

            $image = $dest_path.$filename;
            
            $Ourclients->file=$image;
            
            }

            else
            {
            
            $dest_path = "img/Ourclients/";
            $filename =uniqid()."-".$img->getClientOriginalName();
            
            //resize image
            $img = Image::make($img->getRealPath());
            $img->resize(400, 400);
            $img->save(public_path($dest_path.$filename));

            $image = $dest_path.$filename;
            
            $Ourclients->file=$image;
            
            }

                
        }
        else
        {
            $Ourclients->file=$request->image;

        }

        $Ourclients->group=$request->group;
        $Ourclients->title=$request->title;
        $Ourclients->link=$request->link;
        $Ourclients->status=0;
        $Ourclients->rank=1;
        $Ourclients->caption=$request->caption;
        $Ourclients->keywords=$request->keywords;
        $Ourclients->metaTag=$request->metaTag;
        $Ourclients->metaDescription=$request->metaDescription;
        $Ourclients->user_id=Auth::id();
        $Ourclients->save();
        return redirect('viewclients');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data=Ourclients::find($id);
        $data->delete();
        return redirect('viewclients');
    }


    public function inactivate(Request $request, $id)
    {
        $ourclients=Ourclients::find($id);
        $ourclients->user_id=Auth::id();
        $ourclients->status=0;

        $ourclients->save();
        return redirect('viewclients'); 
    }


    public function activate(Request $request, $id)
    {
        $ourclients=Ourclients::find($id);
        $ourclients->user_id=Auth::id();
        $ourclients->status=1;
        //dd($ourclients);

        $ourclients->save();
        return redirect('viewclients'); 
    }
}
