<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;
use App\User;
use App\Testimonial;
use Auth;
use Input as Input;
use File;
use Image; 


class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $testimonial=new testimonial();

        $img=$request->file;
        //newly added

        if(!is_null($img))
        { 
            $dest_path = "img/testimonial/";
            $filename =uniqid()."-".$img->getClientOriginalName();
            
            //resize image
            $img = Image::make($img->getRealPath());
            $img->resize(500, 500);
            $img->save(public_path($dest_path.$filename));

            $image = $dest_path.$filename;
            
            $testimonial->file=$image;
                
        }

            else
    {
        
    
        return redirect('testimonial');

    }

        $testimonial->fullname=$request->fullname;
        $testimonial->position=$request->position;
        $testimonial->category=$request->category;
        $testimonial->description=$request->description;

        $testimonial->status=0;
        $testimonial->rank=1;
        $testimonial->caption=$request->caption;
        $testimonial->keywords=$request->keywords;
        $testimonial->metaTag=$request->metaTag;
        $testimonial->metaDescription=$request->metaDescription;
        $testimonial->user_id=Auth::id();
        $testimonial->save();
        return redirect('testimonial');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $setting=Setting::all();
        $testimonial=Testimonial::paginate(10);
        $result=Testimonial::find($id);
        //dd($sl);
        return view('back.Testimonial.edit',['testimonial'=>$testimonial,'row'=>$setting,'result'=>$result]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

$testimonial=testimonial::find($id);

        $img=$request->file;
        //newly added

        if(!is_null($img))
        { 
            $file_path= $request->image;
            
            if(file_exists($file_path))
            {
            
            unlink($file_path);
            $dest_path = "img/testimonial/";
            $filename =uniqid()."-".$img->getClientOriginalName();
            
            //resize image
            $img = Image::make($img->getRealPath());
            $img->resize(1400, 750);
            $img->save(public_path($dest_path.$filename));

            $testimonial->file = $dest_path.$filename;
            
            //$testimonial->imagefile=$image;
            
            }

            else
            {
            
            $dest_path = "img/testimonial/";
            $filename =uniqid()."-".$img->getClientOriginalName();
            
            //resize image
            $img = Image::make($img->getRealPath());
            $img->resize(1400, 750);
            $img->save(public_path($dest_path.$filename));

            $testimonial->file = $dest_path.$filename;
            
            //$testimonial->imagefile=$image;
            
            }

                
        }
        else
        {
            $testimonial->file=$request->image;

        }

        $testimonial->fullname=$request->fullname;
        $testimonial->position=$request->position;
        $testimonial->category=$request->category;
        $testimonial->description=$request->description;

        $testimonial->status=0;
        $testimonial->rank=1;
        $testimonial->caption=$request->caption;
        $testimonial->keywords=$request->keywords;
        $testimonial->metaTag=$request->metaTag;
        $testimonial->metaDescription=$request->metaDescription;
        $testimonial->user_id=Auth::id();
        $testimonial->save();
        return redirect('testimonial');



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data=Testimonial::find($id);
        $data->delete();
        
        return redirect('testimonial');
    }


    public function inactivate(Request $request, $id)
    {
        $testimonial=Testimonial::find($id);
        $testimonial->user_id=Auth::id();
        $testimonial->status=0;

        $testimonial->save();
        return redirect('testimonial'); 
    }


    public function activate(Request $request, $id)
    {
        $testimonial=Testimonial::find($id);
        $testimonial->user_id=Auth::id();
        $testimonial->status=1;
        //dd($testimonial);

        $testimonial->save();
        return redirect('testimonial'); 
    }
}
