<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\Setting;
use Auth;
use Input as Input;
use File;
use Image;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $service=new Service();

        $img=$request->file;
        //newly added

        if(!is_null($img))
        { 
            $dest_path = "img/service/";
            $filename =uniqid()."-".$img->getClientOriginalName();
            
            //resize image
            $img = Image::make($img->getRealPath());
            $img->resize(500,500);
            $img->save(public_path($dest_path.$filename));

            $image = $dest_path.$filename;
            
            $service->file=$image;
                
        }

            else
    {
        
    
        return redirect('service');

    }

        $service->title=$request->title;
        $service->link=$request->link;
        $service->description=$request->description;
        $service->extraDescription=$request->extraDescription;
        $service->status=0;
        $service->rank=1;
        $service->caption=$request->caption;
        $service->keywords=$request->keywords;
        $service->metaTag=$request->metaTag;
        $service->metaDescription=$request->metaDescription;
        $service->user_id=Auth::id();
        $service->save();
        return redirect('service');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $setting=Setting::all();
        $service=Service::paginate(9);
        $result=Service::find($id);
        //dd($sl);
        return view('back.service.edit',['row'=>$setting,'service'=>$service,'result'=>$result]);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $service=Service::find($id);

        $img=$request->file;
        //newly added

        if(!is_null($img))
        { 
            $file_path= $request->image;
            
            if(file_exists($file_path))
            {
            
            unlink($file_path);
            $dest_path = "img/service/";
            $filename =uniqid()."-".$img->getClientOriginalName();
            
            //resize image
            $img = Image::make($img->getRealPath());
            $img->resize(500, 500);
            $img->save(public_path($dest_path.$filename));

            $image = $dest_path.$filename;
            
            $service->file=$image;
            
            }

            else
            {
            
            $dest_path = "img/service/";
            $filename =uniqid()."-".$img->getClientOriginalName();
            
            //resize image
            $img = Image::make($img->getRealPath());
            $img->resize(500, 500);
            $img->save(public_path($dest_path.$filename));

            $image = $dest_path.$filename;
            
            $service->file=$image;
            
            }

                
        }
        else
        {
            $service->file=$request->image;

        }
        $service->title=$request->title;
        $service->link=$request->link;
        $service->description=$request->description;
        $service->extraDescription=$request->extraDescription;
        $service->status=0;
        $service->rank=1;
        $service->caption=$request->caption;
        $service->keywords=$request->keywords;
        $service->metaTag=$request->metaTag;
        $service->metaDescription=$request->metaDescription;
        $service->user_id=Auth::id();
        $service->save();
        return redirect('viewservice');



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $data=Service::find($id);
        $data->delete();
        return redirect('viewservice');
    }

     public function inactivate(Request $request, $id)
    {
        $service=Service::find($id);
        $service->user_id=Auth::id();
        $service->status=0;

        $service->save();
        return redirect('viewservice'); 
    }


    public function activate(Request $request, $id)
    {
        $service=Service::find($id);
        $service->user_id=Auth::id();
        $service->status=1;
        //dd($service);

        $service->save();
        return redirect('viewservice'); 
    }   
}
