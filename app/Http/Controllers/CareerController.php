<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Career;
use App\Setting;
use Auth;
use Input as Input;
use File;
use Image;


class CareerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $career=new Career();

        $img=$request->file;
        //newly added

        if(!is_null($img))
        { 
            $dest_path = "img/career/";
            $filename =uniqid()."-".$img->getClientOriginalName();
            
            //resize image
            $img = Image::make($img->getRealPath());
            $img->resize(1400, 750);
            $img->save(public_path($dest_path.$filename));

            $image = $dest_path.$filename;
            
            $career->file=$image;
                
        }

            else
    {
        
    
        return redirect('career');

    }

        $career->filetype=$request->filetype;
        $career->description=$request->description;

        $career->caption=$request->caption;
        $career->keywords=$request->keywords;
        $career->metaTag=$request->metaTag;
        $career->metaDescription=$request->metaDescription;
        $career->user_id=Auth::id();
        $career->save();
        return redirect('career');



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
