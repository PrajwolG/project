<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use auth; //newly added
use App\Slider;
use App\About;
use App\Gallery;
use App\Team;
use App\Service;
use App\ClientGroup;
use App\Ourclients;
use App\Testimonial;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function login()
    {
        return view('auth.login');
    }


    public function logout()
    {
        auth::logout();
        return view('auth.login');
    }





    public function index()
    {
        $slide=Slider::all();
        $about=About::find(1);
        $gallery=Gallery::all();
        $team=Team::all();
        $service=Service::all();
        $group=ClientGroup::all();
        $Ourclients=DB::table('ourclients')
            ->join('client_groups','ourclients.group','=','client_groups.id')
            ->select('client_groups.groups','client_groups.active','ourclients.*')->get();
        $testimonial=Testimonial::all();
        return view('front.home',['slider'=>$slide,'about'=>$about,'gallery'=>$gallery,'team'=>$team,'service'=>$service,'group'=>$group,'Ourclients'=>$Ourclients,'testimonial'=>$testimonial]);
    }





    public function clients()
    {
        $Ourclients=Ourclients::all();
        return view('front.pages.clients',['Ourclients'=>$Ourclients]);
    }

    public function gallery()
    {
        $gallery=Gallery::all();
        return view('front.pages.gallery',['gallery'=>$gallery]);
    }

    public function team()
    {
        $team=Team::all();
        return view('front.pages.team',['team'=>$team]);
    }
}
