<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserCareer;
use Auth;
use Input as Input;
use File;
use Image;

class UserCareerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $UserCareer= new UserCareer();
        $UserCareer->name=$request->fullname;
        $UserCareer->email=$request->email;
        /*$UserCareer->cv=$request->cv;*/
        
        $file=$request->cv;
        //newly added

        if(!is_null($file))
        { 
            $dest_path = "cv/ppimage/";
            $filename =uniqid()."-".$file->getClientOriginalName();
            
            //resize image
            $file = Image::make($file->getRealPath());
           /* $img->resize(500, 400);*/
            $file->save(public_path($dest_path.$filename));

            $UserCareer->cv = $dest_path.$filename;
            
                
        }

            else
    {
        
    
        return redirect('home');

    }


        $img=$request->ppimage;
        //newly added

        if(!is_null($img))
        { 
            $dest_path = "img/ppimage/";
            $filename =uniqid()."-".$img->getClientOriginalName();
            
            //resize image
            $img = Image::make($img->getRealPath());
           /* $img->resize(500, 400);*/
            $img->save(public_path($dest_path.$filename));

            $UserCareer->ppimage = $dest_path.$filename;
            
                
        }

            else
    {
        
    
        return redirect('home');

    }


        
       /* $UserCareer->ppimage=$request->ppimage;*/
        
        $UserCareer->save();
        return redirect('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $data=UserCareer::find($id);
        $data->delete();
        return redirect('UserCareer');
    }
}
